<?php
/**
 * Created by PhpStorm.
 * User: yunli
 * Date: 2019/4/30
 * Time: 上午11:58
 */

use Illuminate\Http\JsonResponse;

/**
 * 显示提示
 * @param $status
 * @param $code
 * @param array $data
 * @return JsonResponse
 */

function tips($status,$code,$message,$data = [])
{
    return response()->json([
        'status' => $status,
        'code' => $code,
        'msg' =>$message,
        'data' => $data,
    ]);
}


/**
 * 验证手机号
 * @param $mobile
 * @return false|string
 */
function checkMobile($mobile)
{
    return preg_match('/^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/i', $mobile);
}


/**
 * 判断是不是json数据
 * true 是， false 不是
 * @param $str
 * @return bool
 */
function is_json($str){
    $flag = is_null(json_decode($str));
    return $flag===true?false:true;
}

function get_month_name($month = ''){
    $month_array =  [
        1 => '一月',
        2 => '二月',
        3 => '三月',
        4 => '四月',
        5 => '五月',
        6 => '六月',
        7 => '七月',
        8 => '八月',
        9 => '九月',
        10 => '十月',
        11 => '十一月',
        12 => '一二月',
    ];
    if($month){
        return $month_array[$month];
    }
    return $month_array;
}

/**
 *
 * @author Bryant
 * @date 2020-10-20 17:29
 *
 * 图片根路径
 */
function img_url(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME']."/storage/";
}


/**
 *
 * @author Bryant
 * @date 2020-10-23 9:42
 *
 * 网站根目录
 */
function app_paths(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
}


/**
 *
 * @param $str
 * @return float|int|mixed|string
 * @author Bryant
 * @date 2020-11-14 13:59
 *
 * 整数
 */
/*function StrToInt($str){
    if (empty($str)){return 0;}
    $symbol=1;
    if ($str{0}=='+'){
        $symbol=1;
        $str{0}='0';
    }
    if ($str[0]=='-'){
        $symbol=-1;
        $str{0}='0';
    }
    $res=0;
    for ($i=0;$i<strlen($str);$i++){
        if($str{$i}<'0' || $str{$i}>'9'){ //包含非数字字符的，直接返回0
            $res=0;
            break;
        }
        $res=$res*10+$str{$i}-'0'; //进位用和ascii相减算出整型数字
    }
    $res=$symbol*$res; //加上正负号
    return $res;
}*/

/**
 *
 * @param $str
 * @return bool|string
 * @author Bryant
 * @date 2020-11-14 13:58
 *
 * 去掉双引号
 */
function remove_quote(&$str) {
    if (preg_match("/^\"/",$str)){
        $str = substr($str, 1, strlen($str) - 1);
    }
    //判断字符串是否以'"'结束
    if (preg_match("/\"$/",$str)){
        $str = substr($str, 0, strlen($str) - 1);;
    }
    return $str;
}

/**
 *
 * @param string $url
 * @param string $type
 * @param array $data
 * @param array $header
 * @return bool|string
 * @author Bryant
 * @date 2020-11-14 13:58
 *
 * CURL 请求
 */
function httpCurl($url = '',$type = 'get',$data = [],$header = []){

    // 初始化
    $ch = curl_init();

    // 设置curl参数
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false); // 绕过ssl
    curl_setopt($ch,CURLOPT_HTTPHEADER, $header); // 添加header信息

    // post提交需要提交参数
    if($type == 'post'){
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
    }

    // 采集
    $result = curl_exec($ch);

    // 关闭
    curl_close($ch);
    return $result;
}

/**
 * 后台图片上传路径
 * @param string $prefix
 * @return string
 */
function adminSavePath($prefix = 'images'){
    return $prefix . '/' . date('Ymd');
}

/**
 *
 * @return string
 * @author Bryant
 * @date 2020-12-02 10:27
 * 链接
 */
function getDomain(){
    return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'];
}


