<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\Customer;
use App\Models\CustomerProgress;
use App\Service\CustomerService;
use App\Service\PartnerExamineService;
use App\Service\PartnerService;
use Illuminate\Http\Request;

class CustomerController extends BaseController
{
    public function __construct()
    {
        // 监控 如果超过时间就删除客户
        $time = time()-5*60;
        $date = date('Y-m-d H:i:s',$time); // 五分钟没有预约到场就删除
        $customer_id = CustomerProgress::where('is_appoint',1)->where('created_at','<',$date)->pluck('customer_id')->toArray();
        CustomerProgress::whereIn('customer_id',$customer_id)->delete();
        Customer::whereIn('id',$customer_id)->delete();
        $this->middleware('members');
    }
    /**
     *
     * @param PartnerExamineService $partnerExamineService
     * @param Request $request
     * @author Bryant
     * @date 2021-04-23 16:03
     *
     */
    public function setCustomer(CustomerService $customerService,Request $request,PartnerService $partnerService)
    {
        $userInfo = $this->userInfos();
        if(!isset($request->mobile,$request->name)) return tips('error',-1,'参数缺失');
        if ($userInfo->is_partner !=2) return tips('error',-1,'您还不是合伙人暂时无法录入客户');
        $partner = $partnerService->getPartner($userInfo->uid);
        $where = [
            'mobile' => $request->mobile
        ];
        if ($customerService->getOne($where)) return tips('error',-1,'该客户已被锁定请添加其他客户');
        $result = $customerService->create($partner->id,$request);
        if (!$result) {
            return tips('error',-1,'添加失败');
        }
        return tips('success',200,'添加成功请尽快联系客户');
    }

    /**
     *
     * @param Request $request
     * @param CustomerService $customerService
     * @author Bryant
     * @date 2021-04-25 11:41
     *
     * 客户列表
     */
    public function customerList(Request $request,CustomerService $customerService,PartnerService $partnerService)
    {
        $userInfo = $this->userInfos();
        if ($userInfo->is_partner !=2) return tips('error',-1,'您不是合伙人');
        $partner = $partnerService->getPartner($userInfo->uid);
        // 总客户
        $count = $partnerService->count($partner->id);
        //今日客户
        $dayCount = $partnerService->dayCount($partner->id);
        // 客户列表
        $customerList = $partnerService->getCustomers($partner->id);
        return tips('success',200,'成功',[
            'count'=>$count,
            'dayCount' => $dayCount,
            'list' => $customerList
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-23 18:54
     * 查看 客户进度
     */
    public function customerProgree(Request $request,CustomerService $customerService)
    {
        $customer_id = $request->customer_id;
        $data = $customerService->getProgress($customer_id);
        return tips('success',200,'成功',$data);
    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2021-04-25 16:20
     *
     * 更新客户进度
     */
    public function updateProgree(Request $request,CustomerService $customerService)
    {
        if (!$request->customer_id) return tips('error',-1,'参数错误');
        $res = $customerService->updateProgress($request->customer_id,$request);
        if (!$res) return tips('error',-1,'失败');
        return tips('success',200,'成功');
    }

}
