<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Goods\goodsInfoRequest;
use App\Models\GoodsModel;
use App\Models\StoreVisitModel;
use App\Service\GoodsOrderService;
use App\Service\GoodsService;
use Illuminate\Http\Request;
use App\Service\GoodsCartService;

class GoodsCartController extends BaseController
{
    public function __construct()
    {
        $this -> middleware('member');
    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-04 18:03
     *
     * 加入购物车
     */
    public function setCart(Request $request)
    {
        $goods_id = $request->goods_id ?? 0;
        $cart_num = $request->cart_num ?? 1;
        $userInfo = $this->userInfo();
        $unique = $request->unique ?? 0;
        if (!$goods_id) {
            return tips('error',-1,'参数错误');
        }
         // 判断商品状态
        $goodsInfo = GoodsService::goodsInfo($goods_id);
        if (!$goodsInfo) {
            return tips('error',-1,'商品状态不允许操作');
        }
        // 判断商品库存
        $stock = GoodsService::getProductStock($goods_id,$unique);
        if ($stock < $cart_num) {

            return tips('errror',-1,'商品库存不足');
        }
        $res = GoodsCartService::setCart($userInfo->uid,$goods_id,$cart_num,$unique);
        if (!$res) {
            return tips('error',-1,'加入失败');
        }
        return tips('success',200,'加入成功');

    }


    /**
     *
     * @author Bryant
     * @date 2020-11-19 11:04
     *
     * 立即购买
     */
    public function nowBuy(Request $request)
    {
        $goods_id = $request->goods_id ?? 0;
        $cart_num = $request->cart_num ?? 1;
        $userInfo = $this->userInfo();
        $unique = $request->unique ?? 0;
        if (!$goods_id) {
            return tips('error',-1,'参数错误');
        }
        // 判断商品状态
        $goodsInfo = GoodsService::goodsInfo($goods_id);
        if (!$goodsInfo) {
            return tips('error',-1,'商品状态不允许操作');
        }
        // 判断商品库存
        $stock = GoodsService::getProductStock($goods_id,$unique);
        if ($stock < $cart_num) {

            return tips('errror',-1,'商品库存不足');
        }
        $res = GoodsCartService::setCart($userInfo->uid,$goods_id,$cart_num,$unique);
        if (!$res) {
            return tips('error',-1,'加入失败');
        }
        return tips('success',200,'加入成功',$res);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-05 10:24
     *
     * 获取用户的购物车
     */
    public function getUserCart()
    {
        // 用户信息
        $userInfo = $this->userInfo();
        $list = GoodsCartService::getCartList($userInfo->uid);
        // 猜你喜欢
        $cate_id = StoreVisitModel::where('uid',$userInfo->uid)->groupBy('cate_id')->get('cate_id')->toArray();

        if ($cate_id) {
            $goodsList = GoodsModel::whereIn('cate_id',$cate_id)->paginate();
        } else {
            // 获取热门推荐的商品
            $where = [
              'is_hot' =>1
            ];
            $goodsList = GoodsService::getGoodsList($where,$userInfo->uid);
        }
        return tips('success',200,'获取成功',[
                'cartList' =>$list,
                'goodsList' => $goodsList,
            ]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-05 17:02
     *
     * 修改用户的购物车数量
     */
    public function editUserCart(Request $request)
    {
        $cart_id = $request -> cart_id;
        $cartNum = $request -> cart_num ?? 1;
        $userInfo = $this->userInfo();
        if (!$cart_id || !$cartNum) {
            return tips('error',-1,'参数错误');
        }
        $cart = GoodsCartService::findCart($cart_id);
        if (!$cart) {
            return tips('error',-1,'参数错误');
        }
        // 获取商品的库存
        $stock = GoodsService::getProductStock($cart->product_id,$cart->product_attr_unique);
        if (!$stock) {
            return tips('error',-1,'库存错误');
        }
        if ($stock < $cartNum) {
            return tips('error',-1,'库存不足');
        }
        $res = GoodsCartService::editUserCart($cart_id,$cartNum,$userInfo->uid);
        if (!$res) {
            return tips('error',-1,'修改失败');
        }
        return tips('success',200,'修改成功');
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-12 9:41
     *
     * 删除订单
     */
    public function delectCart(Request $request)
    {
        $cart_ids = $request->cart_ids;

        $cart_ids = explode(',',$cart_ids);
        $userInfo =  $this->userInfo();

        if (empty($cart_ids)) {

            return tips('error',-1,'请选择要删除的商品');
        }
        $res = GoodsCartService::deleteCart($cart_ids,$userInfo->uid);
        if (!$res) {

            return tips('error',-1,'删除失败');
        }
        return tips('success',200,'删除成功');
    }
}
