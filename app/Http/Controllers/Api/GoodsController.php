<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Models\GoodsModel;
use App\Models\GoodsOrderModel;
use App\Models\StoreVisitModel;
use App\Service\GoodsAttrService;
use App\Service\GoodsCategoryService;
use App\Service\GoodsCollectService;
use App\Service\GoodsCommentService;
use Illuminate\Http\Request;
use App\Service\GoodsService;
use App\Http\Requests\Api\Goods\goodsInfoRequest;

class GoodsController extends BaseController
{

    public function __construct()
    {
        $this -> middleware('member',['except' => ['goodsComments']]);
    }
    /**
     *
     * @param Request $request
     * @param GoodsService $goodsService
     * @author Bryant
     * @date 2020-11-02 16:47
     *
     *  根据筛选条件获取商品
     */
    public function goodsList(Request $request,GoodsService $goodsService)
    {   // 分类id
        $cate_id = $request->input('cate_id',1);
        $is_host = $request->input('is_host',0); // 从热门产品进去的数据
        $is_new  = $request->input('is_new',0); // 从新品上架进去的数据
        $categoryList = GoodsCategoryService::getCate(6); // TODO 六条分类数据
        $userInfo = $this->userInfo();
        $where = [
            'cate_id' => $cate_id,
        ];
        // 获取热门商品
        if ($is_host ==1) {
            $where = [
                'cate_id' => $cate_id,
                'is_hot' => 1,
            ];
        }
        // 获取最新商品
        if ($is_new ==1) {
            $where = [
                'cate_id' => $cate_id,
                'is_new' => 1,
            ];
        }
        $goodsList = $goodsService->getGoodsList($where,$userInfo->uid);
        return tips('success',200,'加载成功',[
            'goodsList' => $goodsList,
            'categoryList' => $categoryList
        ]);

    }


    /**
     *
     * @author Bryant
     * @date 2020-11-17 14:31
     *
     * 搜索商品
     */
    public function searchGoods(Request $request)
    {
        $keyword = $request->keyword;
        $goodsList=GoodsModel::where('store_name','like',"%$keyword%")->orderBy('sort','desc')->paginate(10);
        return tips('success',200,'获取成功',$goodsList);
    }

    /**
     *
     * @param goodsInfoRequest $request
     * @param GoodsService $goodsService
     * @author Bryant
     * @date 2020-11-03 11:52
     * 商品详情
     */
    public function goodsInfo(goodsInfoRequest $request)
    {
        $userInfo = $this->userInfo();
        $goods_id = $request->goods_id;
        $goodsInfo = GoodsService::goodsInfo($goods_id);
        // 判断用户是否收藏
        $goodsInfo['is_collect'] = GoodsCollectService::isCollect($userInfo->uid,$goods_id);
        // 获取商品的规格属性
        list($productAttr,$productValue) = GoodsAttrService::getAttr($goods_id);
        $goodsInfo['attr'] = $productAttr;
        $goodsInfo['attr_value'] = $productValue;
        if (!$goodsInfo) {
            return tips('error',-1,'商品状态不允许操作');
        }
        $where = [
            'product_id' => $goods_id,
            'uid' => $userInfo->uid,
            'cate_id' => $goodsInfo['cate_id'],
        ];
        // 加入用户浏览数据
        $result = StoreVisitModel::where($where)->first();
        if ($result) {
            StoreVisitModel::where($where)->increment('count',1);
        } else {
            $data =[
                'product_id' => $goods_id,
                'uid' => $userInfo->uid,
                'cate_id' => $goodsInfo['cate_id'],
                'count' => 1,
                'type' => 'view',
                'add_time' => time(),
                'product_type' => 'product',
            ];
            StoreVisitModel::insert($data);
        }
        return tips('success',200,'获取成功',$goodsInfo);
    }

    /**
     *
     * @param goodsInfoRequest $request
     * @param GoodsService $goodsService
     * @author Bryant
     * @date 2020-11-03 14:11
     *
     * 获取商品的评论
     */
    public function goodsComments(goodsInfoRequest $request)
    {
        $good_id = $request->goods_id;
        $commentList = GoodsCommentService::goodsComments($good_id);
        return tips('success',200,'获取成功',$commentList);

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-04 16:20
     *
     * 获取所有的商品评论
     */
    public function getAllComments(goodsInfoRequest $request)
    {
        $goods_id = $request->goods_id;
        $count = GoodsCommentService::getAllCount($goods_id); //总数
        $commentList = GoodsCommentService::getAllReplyList($goods_id);
        $goodCount = GoodsCommentService::getGoodCount($goods_id); // 好评
        $finaList = GoodsCommentService::getGoodList($goods_id);
        $imgCount = GoodsCommentService::getImgCount($goods_id);
        $imgList = GoodsCommentService::getImgList($goods_id);
        return tips('success',200,'获取成功',[
            'count' =>$count ?? 0,
            'commentList' => $commentList ?? [],
            'goodCount' => $goodCount ?? 0 ,
            'finaList' => $finaList ?? [],
            'imgCount' => $imgCount ?? 0,
            'imgList' => $imgList ?? [],
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-17 19:26
     *
     * 添加评论
     */
    public function addComment(Request $request)
    {
        $userInfo = $this->userInfo();
        $data = [
            'uid' => $userInfo->uid,
            'oid' => $request->order_id,
            'product_id' => $request->product_id,
            'unique' => $request->unique,
            'product_score' => $request->product_score,
            'comment' => $request->comment,
            'pics' => json_encode($request->pics),
            'add_time' => time(),
        ];

        $res = GoodsCommentService::addComment($data);

        if (!$res) {
            // 订单改变状态
            GoodsOrderModel::where('id',$request->order_id)->update([

            ]);
            return tips('error',-1,'评论失败');
        }

        return tips('success',200,'评论成功');

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-04 14:10
     *
     * 商品收藏
     */
    public function goodsCollect(goodsInfoRequest $request)
    {
        $userInfo = $this->userInfo();
        $goods_id = $request->goods_id;
        $res = GoodsCollectService::collect($userInfo->id,$goods_id);

        if (!$res) {
            return tips('error',-1,'收藏失败');
        }

        return tips('success',200,'收藏成功');
    }

    /**
     *
     * @param goodsInfoRequest $request
     * @author Bryant
     * @date 2020-11-04 14:48
     *  取消收藏
     */
    public function deleteConllect(goodsInfoRequest $request)
    {
        $userInfo = $this->userInfo();
        $goods_id = $request->goods_id;
        $res = GoodsCollectService::deleteCollect($userInfo->id,$goods_id);

        if (!$res) {
            return tips('error',-1,'取消失败');
        }

        return tips('success',200,'取消成功');
    }







}
