<?php

namespace App\Http\Controllers\Api;

use app\ebapi\model\store\StoreCart;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\GoodsCartInfoModel;
use App\Models\GoodsCartModel;
use App\Models\GoodsOrderModel;
use App\Models\UserModel;
use App\Service\GoodsAttrService;
use App\Service\GoodsCartInfoService;
use App\Service\GoodsCartService;
use App\Service\GoodsOrderService;
use App\Service\GoodsOrderStatusService;
use App\Service\GoodsService;
use App\Service\SystemConfigService;
use App\Service\UserBillService;
use App\Service\UserLevelService;
use App\Service\UserAddressService;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use service\HookService;
use service\JsonService;
use Illuminate\Support\Facades\Cache;

class  GoodsOrderController extends BaseController
{
    public function __construct()
    {
        $this->middleware('member');
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-06 14:58
     *
     *  订单页面
     */
    public function orderConfirm(Request $request)
    {
        $cart_id = $request->cart_id;
        $userInfo = $this->userInfo();
        if (!is_string($cart_id) || !$cart_id) return tips('error', -1, '请提交购买的商品');
        // 获取购物车信息
        $cart_id = explode(',', $cart_id);
        $cartGroup = GoodsCartService::getCartList($userInfo->uid, $cart_id);
        if (count($cartGroup['invalid'])) return tips('error', -1, $cartGroup['invalid'][0]['productInfo']['store_name'] . '已失效!');
        if (!$cartGroup['valid']) return tips('error', -1, '请提交购买的商品');
        $cartInfo = $cartGroup['valid'];
        $priceGroup = GoodsService::getOrderPriceGroup($cartInfo);
        $other = [
            'offlinePostage' => SystemConfigService::get('offline_postage'),
            'integralRatio' => SystemConfigService::get('integral_ratio')
        ];
        $data['usableCoupon'] = $usableCoupon ?? 0;
        $data['seckill_id'] = $seckill_id ?? 0;
        $data['cartInfo'] = $cartInfo;
        $data['priceGroup'] = $priceGroup;
        $data['orderKey'] = GoodsOrderService::cacheOrderInfo($userInfo->uid, $cartInfo, $priceGroup);
        $data['offlinePostage'] = $other['offlinePostage'] ?? 0;
        $vipId = UserLevelService::getUserLevel($userInfo->uid);
        $userInfo->vip = $vipId !== false ? true : false;
        if ($userInfo->vip) {
            $userInfo->vip = $vipId;
            $userInfo->vip_discount = UserLevelService::getUserLevelInfo($vipId, 'discount');

        }
        $data['userInfo'] = $userInfo;
        $data['integralRatio'] = $other['integralRatio'];

        return tips('success', 200, '成功返回', $data);
    }


    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-09 17:25
     *
     * 创建订单
     */
    public function createOrder(Request $request)
    {
        try {
            $key = request('key');
            if (!$key) {
                throw new \Exception('error', -1, '参数缺少');
            }
            $userInfo = $this->userInfo();

            $isOrder = GoodsOrderService::getOrder(['unique'=>$key,'uid'=>$userInfo->uid,'is_del'=>0]);
            if ($isOrder) {
                throw new \Exception('订单已存在');
            }
            $payType = request('paytype');// 支付方式
            if (!$payType) {

                throw new \Exception('支付方式选择有误');
            }
            $addressId = request('addressId'); // 地址
            if (!$addressId) {
                throw new \Exception('请选择地址');
            }
            $mark = request('mark', '');//备注
            $self = request('self', 0); //是否自提
            $order = self::cacheKeyCreateOrder($userInfo->uid, $key, $addressId, $payType, $mark,$self);
            $orderId = $order;
            $info = compact('orderId', 'key');
            if ($orderId) {
                $orderInfo = GoodsOrderModel::where('id', $orderId)->first();
                if (!$orderInfo || !isset($orderInfo['paid'])) {
                    throw new \Exception('支付订单不存在');
                }
                if ($orderInfo['paid']) {
                    throw new \Exception('订单已经支付');
                }
                switch ($payType) {
                    case "weixin":
                        // 实例化支付类
                        $app = Factory::payment(config('easywechat.mini_pay'));
                        $jssdk = $app->jssdk;

                        // 微信统一下单
                        $res = $app->order->unify([
                            'body' => '商品购买',
                            'out_trade_no' => $key,
                            'total_fee' =>  $orderInfo['pay_price']* 100,
                            'notify_url' => 'https://x1.kagaro.com/api/wx-notify/goods',
                            'trade_type' => 'JSAPI',
                            'openid' => $userInfo->wx_openid,
                        ]);
                        if ($res['result_code'] == 'FAIL') {
                            throw new \Exception(($res['err_code_des']));
                        }

                        // 获取预支付id后重新签名
                        $res = $jssdk->bridgeConfig($res['prepay_id'], false);
                        return tips('success', 200, '订单创建成功请尽快支付',$res);
                        break;
                    case 'yue':
                        if (self::yuePay($userInfo->uid, $orderId)) {
                            return tips('success', 200, '余额支付成功');
                        } else {
                            return tips('success', 200, '创建订单成功请尽快支付', $info);
                        }
                        break;
                }
            } else {

                throw new \Exception('订单生成失败');
            }
        } catch (\Exception $e) {

            return tips('erorr', -1, $e->getMessage());
        }
    }


    /**
     *
     * @param $uid
     * @param $order
     * @author Bryant
     * @date 2020-11-10 16:41
     * 余额支付
     */
    public static function yuePay($uid, $order_id)
    {
        $orderInfo = GoodsOrderModel::where('uid', $uid)->where('id', $order_id)->where('is_del', 0)->first();
        if (!$orderInfo) throw new \Exception('订单不存在');
        if ($orderInfo['paid']) throw new \Exception('订单已经支付');
        $userInfo = UserModel::where('uid', $uid)->first();
        if ($userInfo['compony_money'] < $orderInfo['pay_price']) throw new \Exception('余额不足');
        \DB::connection()->beginTransaction();
        // 扣除用户余额
        if ($userInfo['compony_id']) {
            $ids = UserModel::where('compony_id',$userInfo['compony_id'])->get('uid');
            $res1 = false !== UserModel::whereIn('uid',$ids)->decrement('compony_money', $orderInfo['pay_price']);
        } else {
            $res1 = false !== UserModel::where('uid',$uid)->decrement('compony_money', $orderInfo['pay_price']);
        }
        // 加入账单明细
        $res2 = UserBillService::expend('购买商品', $uid, 'compony_money', 'pay_product', $orderInfo['pay_price'], $orderInfo['id'], $userInfo['now_money'], '余额支付' . floatval($orderInfo['pay_price']) . '元购买商品', 1, 0);
        // 支付成功回调
        $res3 = self::paySuccess($order_id, 'yue', $formId = '');//支付成功后
        $res = $res1 && $res2 && $res3;
        if ($res) {
            \DB::commit();
            return $res;
        } else {
            \DB::rollBack();
            throw new \Exception('订单余额支付失败');
        }

    }


    /**
     * TODO 支付成功
     * @param $orderId
     * @param string $paytype
     * @param string $formId
     * @return bool
     * @author Bryant
     * @date 2020-11-11 10:37
     *
     * 成功支付
     */
    public static function paySuccess($orderId, $paytype = 'weixin', $formId = '')
    {

        $order = GoodsOrderModel::where('id', $orderId)->first();
        $resPink = true;
        $res1 = GoodsOrderModel::where('id', $orderId)->update(['paid' => 1, 'pay_type' => $paytype, 'pay_time' => time()]);//订单改为支付
        $cartInfo = GoodsCartInfoModel::where('oid', $order['id'])->get('cart_info', 'unique')->toArray() ?: [];
        foreach ($cartInfo as $k => &$cart) $cart['cart_info'] = json_decode($cart['cart_info'], true);
        $res2 = true;
        foreach ($cartInfo as $k => &$cart) {  //减库存加销量
            $res2 = GoodsAttrService::decProductStock($cart['cart_info']['cart_num'], $cart['cart_info']['productInfo']['id'], isset($cart['cart_info']['productInfo']['attrInfo']) ? $cart['cart_info']['productInfo']['attrInfo']['unique'] : '');
        }
        UserModel::where('uid', $order['uid'])->increment('pay_count', 1);// 用户购买次数加一

        GoodsOrderStatusService::status($order['id'], 'pay_success', '用户付款成功'); // 记录订单状态

        //添加成为推荐人的逻辑的判断
        $user_is_promoter = UserModel::where('uid', $order['uid'])->first();
        if ($user_is_promoter['is_promoter']) {
            // 加入推荐员的奖励
            $agent_id = UserModel::where('uid', $order['uid'])->value('spread_uid');

            if ($agent_id) {
                $getType = SystemConfigService::get('type_reword');// 1比例 2 是直接奖励
                $getType = remove_quote($getType);
                $getType = StrToInt($getType);
                if ($getType == 1) {
                    // 给一级加上对应奖励的钱
                    $rate = SystemConfigService::get('fen_add');
                    (int)$rate = remove_quote($rate);
                    (float)$money = $rate * $order['pay_price'] / 100;// 一级奖励比例

                } else {
                    $money = SystemConfigService::get('fen_add');
                    (float)$money = remove_quote($money);

                }
                UserModel::where('uid', $agent_id)->increment('now_money', $money);
                $userInfo = UserModel::where('uid', $order['uid'])->first();
                $mark = $userInfo['nickname'] . '成功消费' . floatval($order['pay_price']) . '元,奖励推广佣金' . floatval($money);
                UserBillService::expend('获得推广佣金', $agent_id, 'now_money', 'brokerage', $money, $order['id'], $userInfo['now_money'], $mark, 1, 1, $order['uid']);
                $second_agent = UserModel::where('uid', $agent_id)->value('spread_uid'); // 二级奖励
                if ($second_agent) {
                    $getType = SystemConfigService::get('type_reword');// 1比例 2 是直接奖励
                    $getType = remove_quote($getType);
                    $getType = StrToInt($getType);
                    if ($getType == 1) {
                        // 给一级加上对应奖励的钱
                        $rate = SystemConfigService::get('san');
                        (int)$rate = remove_quote($rate);
                        (float)$money = $rate * $order['pay_price'] / 100;// 一级奖励比例
                    } else {
                        $money = SystemConfigService::get('fen_add');
                        (float)$money = remove_quote($money);
                    }
                    UserModel::where('uid', $second_agent)->increment('now_money', $money);
                    $userInfo = UserModel::where('uid', $order['uid'])->first();
                    $mark = $userInfo['nickname'] . '成功消费' . floatval($order['pay_price']) . '元,奖励推广佣金' . floatval($money);
                    UserBillService::expend('获得推广佣金', $second_agent, 'now_money', 'brokerage', $money, $order['id'], $userInfo['now_money'], $mark, 1, 1, $order['uid']);
                }

            }
        }

        $res = $res1 && $resPink && $res2;
        return false !== $res;
    }


    /**
     *
     * @param $uid
     * @param $key
     * @param $addressId
     * @param $payType
     * @param bool $useIntegral
     * @param int $couponId
     * @param string $mark
     * @param int $combinationId
     * @param int $pinkId
     * @param int $seckill_id
     * @param int $bargain_id
     * @return mixed
     * @author Bryant
     * @date 2020-11-10 11:42
     *
     *  创建订单
     */
    public static function cacheKeyCreateOrder($uid, $key, $addressId, $payType, $mark = '', $self = 0)
    {

        $cartGroup = self::getCacheOrderInfo($uid, $key);
        if (!$cartGroup) throw new \Exception('请选择提交的商品');
        $cartInfo = $cartGroup['cartInfo'];
        $priceGroup = $cartGroup['priceGroup'];
        $payPrice = (float)$priceGroup['totalPrice'];
        $addressInfo = UserAddressService::getDetailt($uid, $addressId);
        if (!$addressInfo) {
            throw new \Exception('请选择一个地址');
        }
        $res1 = true;
        $cartIds = [];
        $totalNum = 0;
        $gainIntegral = 0;
        foreach ($cartInfo as $cart) {
            $cartIds[] = $cart['id'];
            $totalNum += $cart['cart_num'];
            $gainIntegral = bcadd($gainIntegral, isset($cart['productInfo']['give_integral']) ? $cart['productInfo']['give_integral'] : 0, 2);
        }
        $cart_ids = json_encode($cartIds);
        $orderInfo = [
            'uid' => $uid,
            'order_id' => GoodsOrderService::getNewOrderId(),
            'real_name' => $addressInfo['real_name'],
            'user_phone' => $addressInfo['phone'],
            'user_address' => $addressInfo['province'] . ' ' . $addressInfo['city'] . ' ' . $addressInfo['district'] . ' ' . $addressInfo['detail'],
            'cart_id' => $cart_ids,
            'total_num' => $totalNum,
            'total_price' => $priceGroup['totalPrice'],
            'total_postage' => $priceGroup['storePostage'],
            'coupon_id' => $couponId ?? 0,
            'coupon_price' => $couponPrice ?? 0,
            'pay_price' => $payPrice,
            'pay_postage' => $payPostage ?? 0,
            'deduction_price' => $deductionPrice ?? 0,
            'paid' => 0,
            'pay_type' => $payType,
            'use_integral' => $usedIntegral ?? 0,
            'gain_integral' => $gainIntegral,
            'mark' => htmlspecialchars($mark),
            'combination_id' => $combinationId ?? 0,
            'pink_id' => $pinkId ?? 0,
            'seckill_id' => $seckill_id ?? 0,
            'bargain_id' => $bargain_id ?? 0,
            'cost' => $priceGroup['costPrice'],
            'is_channel' => 1,
            'unique' => $key,
            'selfmention' => $self,
        ];

        \DB::connection()->beginTransaction();
        $order = GoodsOrderService::createOrder($orderInfo);
        if (!$order) throw new \Exception('订单生成失败');
        $res5 = true;
        //保存购物车商品信息
        $res4 = false !== GoodsCartInfoService::setCartInfo($order, $cartInfo);
        //购物车状态修改
        $res6 = false !== GoodsCartModel::whereIn('id', $cartIds)->update(['is_pay' => 1]);
        if (!$res4 || !$res5 || !$res6) throw new \Exception('订单生成失败');
        self::clearCacheOrderInfo($uid, $key);
        GoodsOrderStatusService::status($order, 'cache_key_create_order', '订单生成');
        if ($res = $res4 && $res5 && $res6) {
            \DB::commit();
            return $order;
        } else {
            \DB::rollBack();
            throw new \Exception('订单生成失败');
        }

    }

    /**
     *
     * @param $uid
     * @param $key
     * @return |null
     * @author Bryant
     * @date 2020-11-10 11:45
     *
     * 获取缓存信息
     */
    public static function getCacheOrderInfo($uid, $key)
    {
        $cacheName = 'user_order_' . $uid . $key;
        if (!Cache::has($cacheName)) return null;
        return Cache::get($cacheName);
    }

    /**
     *
     * @param $uid
     * @param $key
     * @author Bryant
     * @date 2020-11-10 14:26
     *
     */
    public static function clearCacheOrderInfo($uid, $key)
    {
        Cache::forget('user_order_' . $uid . $key);
    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-13 13:42
     *
     * 我的商城订单
     */
    public function getOrderList(Request $request)
    {
        $userInfo = $this->userInfo();

        $order_status = $request->order_status;
        $paid = $request->paid;
        $where = [];
        $where[] = ['status', '=', $order_status];
        if (in_array($paid,[0,1])) {
            $where[] = ['paid', '=', $paid];
        }
        $list = GoodsOrderService::getOrderList($userInfo->uid, $where);
        return tips('success', 200, '获取成功', [
            'list' => $list
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-14 10:46
     *
     *
     * 再次购买
     */
    public function payAgain(Request $request)
    {
        $unique = request('unique'); // 参数
        $userInfo = $this->userInfo();
        if (!$unique) {
            return tips('error',-1,'参数错误');
        }
        $order = GoodsOrderModel::where('unique',$unique)->first();

        if (!$order) {
            return tips('error',-1,'订单不存在');
        }
        $order = GoodsOrderService::payAgain($order,true,true);
        foreach ($order['cartInfo'] as $v) {

            if($v['cart_info']['combination_id'])  return tips('error',-1,'拼团产品不能再来一单，请在拼团产品内自行下单!');
            else if($v['cart_info']['bargain_id']) return tips('error',-1,'砍价产品不能再来一单，请在砍价产品内自行下单!');
            else if($v['cart_info']['seckill_id'])  return tips('error',-1,'秒杀产品不能再来一单，请在砍价产品内自行下单!');
            else $res[] = GoodsCartService::setCart($userInfo->uid, $v['cart_info']['product_id'], $v['cart_info']['cart_num'], isset($v['cart_info']['productInfo']['attrInfo']['unique']) ? $v['cart_info']['productInfo']['attrInfo']['unique'] : '', 'product', 0, 0);
        }
        $cartId = [];
        foreach ($res as $v){
            if(!$v) return tips('error',-1,'再来一单失败，请重新下单!');
            $cartId[] = $v;
        }
        return tips('success', 200, '再次下单成功',$cartId);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-18 14:23
     *
     * 订单支付
     */
    public function payOrder(Request $request)
    {
        $userInfo = $this->userInfo();
        $unique = $request->unique;
        if (!$unique) {
            return tips('error',-1,'参数出错');
        }
        // 查询订单的状态
        $order = GoodsOrderModel::where('unique',$unique)->first();
        if (!$order) {
            return tips('error',-1,'订单不存在');
        }
        if ($order['paid']) {
            return tips('error',-1,'订单已经支付');
        }
        $pay_type = $request->pay_type;
        switch ($pay_type) {
            case "weixin":
                // 实例化支付类
                $app = Factory::payment(config('easywechat.mini_pay'));
                $jssdk = $app->jssdk;

                // 微信统一下单
                $res = $app->order->unify([
                    'body' => '商品购买',
                    'out_trade_no' => $order['unique'],
                    'total_fee' => $order['pay_price'] * 100,
                    'notify_url' => 'http://daishu.gdweihu.com/api/wx-notify/goods',
                    'trade_type' => 'JSAPI',
                    'openid' => $userInfo->wx_openid,
                ]);
                if ($res['result_code'] == 'FAIL') {
                    throw new \Exception(($res['err_code_des']));
                }
                // 获取预支付id后重新签名
                $res = $jssdk->bridgeConfig($res['prepay_id'], false);
                $order->pay_type = $pay_type;
                $order->save();
                return tips('success', 200, '请尽快支付',$res);
                break;
            case 'yue':
                if (self::yuePay($userInfo->uid, $order['id'])) {
                    $order->pay_type = $pay_type;
                    $order->save();
                    return tips('success', 200, '余额支付成功');
                } else {
                    $order->pay_type = $pay_type;
                    $order->save();
                    return tips('success', 200, '创建订单成功请尽快支付');
                }
                break;
        }

    }


    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-18 15:40
     *
     * 确认收货
     */
    public function queOrder(Request $request)
    {
        $unique = $request->unique;
        if (!$unique) {
            return tips('error',-1,'参数错误');
        }
        // 查询订单的状态
        $order = GoodsOrderModel::where('unique',$unique)->first();
        if (!$order) {
            return tips('error',-1,'订单不存在');
        }
        if ($order['status'] !=1) {
            return tips('error',-1,'订单状态不允许操作');
        }
        $res = GoodsOrderModel::where('unique',$unique)->update([
            'status' => 2 //待评价
        ]);

        if (!$res) {
            return tips('error',-1,'收货失败');
        }

        return tips('success',200,'收货成功');
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-19 14:26
     *
     * 删除订单
     */
    public function deleteOrder()
    {
        $unique = request('unique');

        // 查询订单的状态
        $order = GoodsOrderModel::where('unique',$unique)->first();
        if (!$order) {
            return tips('error',-1,'订单不存在');
        }
        if ($order['status'] !=0 && $order['paid'] !=0) {
            return tips('error',-1,'订单状态不允许操作');
        }
        $res = GoodsOrderModel::where('unique',$unique)->update([
            'is_del' => 1 //删除
        ]);

        if (!$res) {
            return tips('error',-1,'删除失败');
        }

        return tips('success',200,'删除成功');

    }

}