<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Service\GroupDataService;
use App\Service\SystemConfigService;
use http\Url;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Index extends Controller
{
    //
    public function Index()
    {
        $limit = 0;
        $banner = GroupDataService::getData('routine_home_banner', $limit) ?? [];//TODO 首页banner图
        $new = GroupDataService::getData('routine_home_roll_news', $limit) ?? [];//TODO 首页banner图
        return tips('success', 200, '获取成功', [
            'banner' => $banner,
            'new' => $new,
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2021-05-11 16:12
     *
     * 合伙人小程序首页
     */
    public function Indexs()
    {
        $limit = 0;
        $banner = GroupDataService::getData('ware_banner', $limit) ?? [];//TODO 首页banner图
        $new = GroupDataService::getData('partner_new', $limit) ?? [];//TODO 首页banner图
        return tips('success', 200, '获取成功', [
            'banner' => $banner,
            'new' => $new,
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-12-30 14:55
     *
     * 公司余额
     */
    public function getUrl()
    {
        //$http_response_header[] ='application/x-www-form-urlencoded';
        $mobile = request('mobile');
        $json = array('mobile' => $mobile);
        $url = 'https://w2.kagaro.com/xcx/user/login_mobile';
        //$arr= ['mobile'=>$mobile];
        $res = httpCurl($url, 'post', $json);
        $res = json_decode($res, true);
        if ($res['code'] == 200) {
            $compony_id = $res['data']['customer_id'];

            if ($compony_id) {

            }
        }
    }

    /**
     *
     * @author Bryant
     * @date 2021-01-05 14:16
     *
     * 联系我们
     */
    public function linkUs()
    {
        $list['compony_name'] = json_decode(SystemConfigService::get('compony_name'));
        $list['compony_address'] = json_decode(SystemConfigService::get('compony_address'));
        $list['site_email'] = json_decode(SystemConfigService::get('site_email'));
        $list['site_phone'] = json_decode(SystemConfigService::get('site_phone'));
        return tips('success', 200, '获取成功', $list);

    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2021-01-06 10:53
     *
     * 获取经纬度
     */
    public function getLat(Request $request)
    {
        $address = request('address');
        $key = '7ZJBZ-A2HRW-7YVRB-RQBF3-P5TPH-72BEE';
        $url = 'https://apis.map.qq.com/ws/geocoder/v1/?address=' . $address . '&key=' . $key;
        $info = file_get_contents($url);
        $info = json_decode($info, true);
        return tips('success', 200, '获取成功', $info);
    }


    /**
     *
     * @author Bryant
     * @date 2021-03-24 17:47
     *
     * 获取仓库经纬度
     */
    public function getLocation(Request $request)
    {
        $name = $request->name;
        if (!$name) {
            return tips('error', '-1', '参数错误');
        }
        $result = Location::where('dec', $name)->first();

        if ($result) {
            return tips('success', '200', '成功', $result);
        } else {
            return tips('error', '-1', '暂无信息请联系管理员');
        }
    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2021-04-06 10:13
     *
     * 检查用户
     */
    public function checkUser(Request $request)
    {
        $mobile = $request->mobile;
        $url = 'https://w2.kagaro.com/xcx/user/check_wx';
        //$result =
    }

    public function getArea(Request $request)
    {
        $city = $request->city;
        $city_id = DB::table('fa_recovery_city')->where('name', $city)->first();
        $area = DB::table('fa_recovery_district')->where('city_id', $city_id->id)->get();
        return tips('success', 200, '成功', $area);
    }

    /**
     *
     * @author Bryant
     * @date 2021-05-26 16:22
     *
     * 获取图片
     */
    public function getImage()
    {
        $limit = 0;
        $image = GroupDataService::getData('index_image', $limit) ?? [];//TODO 首页跳转图
        return tips('success',200,'成功',$image);

    }
}
