<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\UserPartnerModel;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\Api\Login\LoginRequest;
use App\Lib\OpenWechat;
use Tymon\JWTAuth\Factory;
use EasyWeChat\Factory as Fac;


class LoginController extends BaseController
{
    public function Login(LoginRequest $request,UserModel $usermodel)
    {
        try{

            $username = $request->username;

            $res = $usermodel-> where('phone',$username)->first();

            if ($res) {
                $token = JWTAuth::fromUser($res);
            }
            dd($token);
        } catch (\Exception $e){

            return tips('error',-1,$e->getMessage());
        }

    }

    public function Logins(LoginRequest $request,UserPartnerModel $userPartnerModel)
    {
        try{

            $username = $request->username;

            $res = $userPartnerModel-> where('phone',$username)->first();

            if ($res) {
                $token = JWTAuth::fromUser($res);
            }
            dd($token);
        } catch (\Exception $e){

            return tips('error',-1,$e->getMessage());
        }

    }

    /**
     *
     * @param Request $request
     * @throws \Exception
     * @author Bryant
     * @date 2020-11-14 14:37
     *
     *
     * 微信登录
     */
    public function wxLogin(Request $request,UserModel $usermodel)
    {
        try{

            $code = request('code');
            $open = new OpenWechat(config('app.WX_OPEN_APP_ID'),config('app.WX_OPEN_APP_SECRET'));
            $data = $open -> getAccessToken($code);
            // 查找是否存在
            $memberObj = UserModel::where('wx_openid','=',$data['openid']) -> first();
            // 不存在则注册
            if (!$memberObj) {
                // 解密用户的手机号
                $iv = request('iv');
                $encryptedData = request('encryptedData');
                $nickname = request('nickname');
                $avatar = request('avatar');
                //$agent_id = request('uid');
                $errCode = $open->decryptData($data['session_key'],$encryptedData, $iv,  $decryptData);
                $decryptData = json_decode($decryptData,true);
                if ($errCode == 0) {
                    $usermodel->account = "wx".date('md').time();
                    $usermodel->nickname = $nickname;
                    $usermodel->phone = $decryptData['phoneNumber'];
                    $usermodel->add_time = time();
                    $usermodel->wx_openid = $data['openid'];
                    $usermodel-> avatar = $avatar;
                    $mobile = $decryptData['phoneNumber'];
                    $json = array('mobile'=>$mobile);
                    $url = 'https://w2.kagaro.com/xcx/user/login_mobile';
                    //$arr= ['mobile'=>$mobile];
                    $res = httpCurl($url,'post',$json);
                    $res = json_decode($res,true);
                    if ($res['code'] == 200) {
                        $compony_id = $res['data']['customer_id'];
                        if ($compony_id) {
                            $result = UserModel::where('compony_id',$compony_id)->first();
                            if ($result) {
                                $usermodel -> compony_money = $result->compony_money ?? 0;
                            }
                            $usermodel-> compony_id = $compony_id ?? 0;
                        }
                    }
                    $usermodel->user_type = 'wechat';
                    $res = $usermodel->save();
                    if ($res) {
                        $member = $usermodel->where('uid',$usermodel->uid)->first();
                        $token = JWTAuth::fromUser($member);
                        return tips('success',200,'登录成功',[
                            'token' => $token,
                            'expire_time' => config('app.JWT_TTL')
                        ]);
                    }else {
                        return tips('error',-1,'登录失败');
                    }
                }
            }
            $mobile = $memberObj->phone;
            $json = array('mobile'=>$mobile);
            $url = 'https://w2.kagaro.com/xcx/user/login_mobile';
            //$arr= ['mobile'=>$mobile];
            $res = httpCurl($url,'post',$json);
            $res = json_decode($res,true);
            if ($res['code'] == 200) {
                $compony_id = $res['data']['customer_id'];
                if ($compony_id) {
                    $result = UserModel::where('compony_id',$compony_id)->first();
                    if ($result) {
                        $memberObj -> compony_money = $result->compony_money ?? 0;
                    }
                    $memberObj-> compony_id = $compony_id ?? 0;
                }
            }
            $memberObj->last_time  = time();
            $memberObj->last_ip = $_SERVER['REMOTE_ADDR'];
            $memberObj->save();
            $token = JWTAuth::fromUser($memberObj);
            return tips('success',200,'登录成功',[
                'token' => $token,
                'expire_time' => config('app.JWT_TTL')
            ]);
        }catch(\Exception $e){

            return tips('error',-1,$e->getMessage());
        }

    }

    public function wxLogins(Request $request,UserPartnerModel $usermodel)
    {
        try{

            $code = request('code');
            $open = new OpenWechat(config('app.WX_OPEN_APP_IDS'),config('app.WX_OPEN_APP_SECRETS'));
            $data = $open -> getAccessToken($code);
           /* $code = request('code');
            $config = [
                'app_id' => 'wxedbd4180ce089936',
                'secret' => '3e1db2f92399211c2e838922249e924d',

                // 下面为可选项
                // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
                'response_type' => 'array',

                'log' => [
                    'level' => 'debug',
                    'file' => __DIR__.'/wechat.log',
                ],
            ];*/
           /* $iv = request('iv');
            $encryptedData = request('encryptedData');*/
           /* $app = Fac::miniProgram($config);
            $res = $app->auth->session($code);*/
            //dd($res);
            //$app->encryptor->decryptData($session, $iv, $encryptedData);
            // 查找是否存在
            $memberObj = UserPartnerModel::where('wx_openid','=',$data['openid']) -> first();
            // 不存在则注册
            if (!$memberObj) {
                // 解密用户的手机号
                //$iv = request('iv');
                //$encryptedData = request('encryptedData');
                $nickname = request('nickname');
                $avatar = request('avatar');
                //$agent_id = request('uid');
                //$errCode = $open->decryptData($data['session_key'],$encryptedData, $iv,  $decryptData);
                //$decryptData = json_decode($decryptData,true);
                //if ($errCode == 0) {
                    $usermodel->account = "wx".date('md').time();
                    $usermodel->nickname = $nickname;
                    //$usermodel->phone = $decryptData['phoneNumber'] ?? 0;
                    $usermodel->add_time = time();
                    $usermodel->wx_openid = $data['openid'];
                    $usermodel-> avatar = $avatar;
                    $usermodel->user_type = 'wechat';
                    $res = $usermodel->save();
                    if ($res) {
                        $member = $usermodel->where('uid',$usermodel->uid)->first();
                        $token = JWTAuth::fromUser($member);
                        return tips('success',200,'登录成功',[
                            'token' => $token,
                            'expire_time' => config('app.JWT_TTL')
                        ]);
                    }else {
                        return tips('error',-1,'登录失败');
                    }
                }
            //}
            $memberObj->last_time  = time();
            $memberObj->last_ip = $_SERVER['REMOTE_ADDR'];
            $memberObj->save();
            $token = JWTAuth::fromUser($memberObj);
            return tips('success',200,'登录成功',[
                'token' => $token,
                'expire_time' => config('app.JWT_TTL')
            ]);
        }catch(\Exception $e){

            return tips('error',-1,$e->getMessage());
        }

    }
}
