<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Service\PartnerExamineService;
use Illuminate\Http\Request;

class PartnerExamineController extends BaseController
{
    public function __construct()
    {
        $this->middleware('members');
    }
    /**
     *
     * @param PartnerExamineService $partnerExamineService
     * @param Request $request
     * @author Bryant
     * @date 2021-04-23 16:03
     *
     */
    public function setPartner(PartnerExamineService $partnerExamineService,Request $request)
    {
        $userInfo = $this->userInfos();
        if ($userInfo->is_partner == 2) return tips('error',-1,'您已经是合伙人了，请勿重复提交');
        if(!isset($request->mobile,$request->name,$request->address,$request->type)) return tips('error',-1,'参数缺失');
        if($partnerExamineService->getOne($userInfo->uid,$request->mobile)) return tips('error',-1,'您有记录请勿重复提交');
        $result = $partnerExamineService->create($userInfo->uid,$request);
        if (!$result) {
            return tips('error',-1,'添加失败');
        }
        return tips('success',200,'添加成功请等待审核');
    }


}
