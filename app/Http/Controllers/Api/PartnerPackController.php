<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Service\PartnerExamineService;
use App\Service\UserBillService;
use App\Service\UserExtractService;
use Illuminate\Http\Request;

class PartnerPackController extends BaseController
{
    public function __construct()
    {
        $this->middleware('members');
    }

    /**
     *
     * @param Request $request
     * @param UserBillService $billService
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2021-05-10 16:27
     *
     * 钱包
     */
    public function getIncome(Request $request,UserBillService $billService)
    {
        $userInfo = $this->userInfos();
        $list = $billService::getIncome($userInfo->uid);
        return tips('success',200,'成功',[
            'userInfo' => $userInfo,
            'list' => $list,
        ]);
    }

    /**
     *
     * @param Request $reqest
     * @param UserExtractService $extractService
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2021-05-15 15:58
     *
     * 获取用户的提现记录
     */
    public function getExtract(Request $reqest,UserExtractService $extractService)
    {
        $userInfo = $this->userInfos();
        $list = $extractService->getExtract($userInfo->uid);
        return tips('success',200,'成功',$list);
    }

    /**
     *
     * @param Request $request
     * @param UserExtractService $extractService
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2021-05-15 16:59
     *
     * 申请提现
     */
    public function createExtract(Request $request,UserExtractService $extractService)
    {
        $userInfo = $this->userInfos();
        $uid = $userInfo->uid;
        $res = $extractService->create($uid,$request);
        if (!$res) return tips('error',-1,'申请失败');
        return tips('success',200,'成功');
    }


}
