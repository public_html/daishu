<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Service\GoodsCategoryService;
use App\Service\GoodsService;
use Illuminate\Http\Request;
use App\Service\BannerService;
use App\Service\GroupDataService;

class ShopController extends BaseController
{
    /**
     *
     * @param BannerService $bannerService
     * @param GoodsService $goodsService
     * @param GoodsCategoryService $goodsCategoryService
     * @return mixed
     * @author Bryant
     * @date 2020-11-03 9:51
     *
     * 获取首页Banner 分类 热门 新品商品
     */
    public function getIndex(Request $request,BannerService $bannerService,GoodsService $goodsService,GoodsCategoryService $goodsCategoryService)
    {
        $limit = $request->limit ?? 5;
        $banner = GroupDataService::getData('store_home_banner',$limit) ?? [];//TODO 首页banner图
        // 获取商品分类
        $categoryList = $goodsCategoryService->getCate(6); // TODO 首页的分类
        // 获取热门商品
        $hotList = GroupDataService::getData('routine_home_hot_banner',$limit) ?? [];//TODO 首页热门图
        // 获取新品
        $newList = GroupDataService::getData('routine_home_new_banner',$limit) ?? [];//TODO 首页新品图
        $goodsList = $goodsService->getAllGoods(); // TODO 获取所有商品
        return tips('success',200,'加载成功',[
            'banner' => $banner,
            'categoryList' => $categoryList,
            'hotList' => $hotList,
            'newList' => $newList,
            'goodsList' => $goodsList
        ]);

    }

    public function getAllCate()
    {
        $categoryList = GoodsCategoryService::getCate(); // TODO 分类

        return tips('success',200,'获取成功',$categoryList);
    }

    /**
     *
     * @param Request $request
     * @param GoodsService $goodsService
     * @return mixed
     * @author Bryant
     * @date 2020-11-03 10:09
     *
     *  获取下方的商品列表
     */
    public function getGoods(Request $request,GoodsService $goodsService)
    {
        $type = $request->input('type',1);// 推荐的商品

        if ($type == 1) {

            $where = [
                'is_best' => 1,
            ];
            $order = 'add_time desc';
            $goodsList = $goodsService ->getGoodsList($where,$order);
        } else {
            $where = [
                'is_new' => 1,
            ];
            $order = 'add_time desc';
            $goodsList = $goodsService ->getGoodsList($where,$order);

        }

        return tips('success',200,'加载成功',$goodsList);
    }
}