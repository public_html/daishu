<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\StockOrderModel;
use App\Models\UserModel;
use App\Service\StockOrderService;
use App\Service\StockService;
use App\Models\StockModel;
use App\Service\SystemConfigService;
use App\Service\UserBillService;
use App\Service\UserStockService;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends BaseController
{
    public function __construct()
    {
        // 初始化 将过期的仓票改成已过期
        $month = date('n');
        $year = date('Y');
        $stock_id = StockModel::where('use_year','<=',$year)
            ->where('use_month','<',$month)
            ->where('status',1)
            ->get('id');
        if ($stock_id) {
            StockModel::whereIn('id',$stock_id)->update([
                'status'=>2,
            ]);
        }
        $this -> middleware('member',['except' => ['getStock','getStockInfo']]);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-23 18:46
     *
     *
     */
    public function getStock(Request $request)
    {
        $month = $request->month ?? 0;
        $year = $request->year;
        $where = [];
        if ($month && $year) {
            $where = [
                'use_month'=>$month,
                'use_year' => $year,
            ];
        }
        $list = StockService::getStock($where);

        return tips('success',200,'获取成功',$list);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-23 18:55
     *
     * 获取仓票详情
     */
    public function getStockInfo(Request $request)
    {
        $stock_id = $request->stock_id; // 仓票id

        if (!$stock_id) {
            return tips('error',-1,'参数错误');
        }
        // 获取详情
        $info = StockService::getStockInfo($stock_id);
        return tips('success',200,'获取成功',$info);

    }


    /**
     *
     * @author Bryant
     * @date 2020-11-23 19:16
     *
     * 仓票下单
     */
    public function orderStock(Request $request)
    {
        try {
            $stock_id = $request->stock_id;
            $userInfo = $this->userInfo();
            if (!$stock_id) {
                return tips('error',-1,'参数错误');
            }
            $pay_type = $request->pay_type;
            if (!$pay_type) {
                return tips('error',-1,'请选择支付方式');
            }
            $info = StockService::getStockInfo($stock_id);
            if (!$info) {
                return tips('error',-1,'仓票状态不允许操作');
            }
            //if ($info['num'] < $request->num) return tips('error',-1,'仓票数量不足');
            $stockOrder = new StockOrderModel();
            $stockOrder -> uid = $userInfo ->uid;
            $stockOrder -> stock_id = $stock_id;
            $stockOrder -> get_money = $info['get_money'];
            $stockOrder -> pay_money = $info['pay_money'];
            $stockOrder -> use_month = $info['use_month'];
            $stockOrder -> use_year = $info['use_year'];
            $stockOrder -> status = 0; //初始化
            $stockOrder -> pay_type = $pay_type;
            $stockOrder -> created_at = date('Y-m-d H:i:s',time());
            $stockOrder -> unique = 'ST'.time().mt_rand(00000,99999);
            $stockOrder -> save();
            $order_id = $stockOrder->id;
            if (!$order_id) {
                throw new \Exception('下单失败');
            }
            return tips('success',200,'下单成功',[
                'order_id' => $order_id,
                'pay_money' =>  $stockOrder -> pay_money,
                'info' => $info,
            ]);
        }catch(\Exception $e){
            return tips('error',-1,$e->getMessage());
        }

    }

    /**
     *
     * @param $info
     * @author Bryant
     * @date 2020-11-24 11:35
     *
     * 获取真实的价格
     */
    public function getRealMoney($stock_id)
    {
        // 获取真实的价格
        if (!$stock_id) {
            throw new \Exception('参数错误');
        }
        $info = StockService::getStockInfo($stock_id);
        $month = date('n'); // 当前的月份
        // 根据年份判断明年的月份的数据
        if ($info['use_month'] >= $month) {
            $num = $info['use_month']-$month;
            $discount = $this->getDiscount($num);
        } else {
            // 和12月的比较
            $num = (12-$month)+$info['use_month'];
            $discount = $this->getDisCount($num);
        }

        $real_money = $info['pay_money']*$discount/100;
        return $real_money;

    }

    /**
     *
     * @param $num
     * @author Bryant
     * @date 2020-11-24 14:26
     *
     * 获取折扣
     */
    public function getDiscount($num)
    {
        $nums = $num;
        $setting= SystemConfigService::get('pre_dec'); //获取后台设置的递减比例
        $setting = (int)$setting;
        // 计算表达式
        switch($nums) {
            case 1:
                $discount = 100-$setting*$nums;
                break;
            case 2:
                $discount = 100-$setting*$nums;
                break;
            case 3:
                $discount = 100-$setting*$nums;
                break;
            case 4:
                $discount = 100-$setting*$nums;
                break;
            case 5:
                $discount = 100-$setting*$nums;
                break;
            default:
                $discount = 100;

        }
        return $discount;
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-24 15:00
     *
     * 仓票支付
     */
    public function payStock(Request $request)
    {
        try {
            // 开启事务
            \DB::beginTransaction();
            $userInfo = $this->userInfo(); // 获取用户信息
            $order_id = $request->order_id;
            if (!$order_id) {
                return tips('erroe',-1,'参数错误');
            }
            $orderInfo = StockOrderService::getOrderInfo($order_id);
            if (!$orderInfo) {
                return tips('error',-1,'订单不存在');
            }
            if ($orderInfo['status'] == 1) {
                return tips('error',-1,'订单已经支付');
            }

            $num = StockModel::where('id',$orderInfo->stock_id)->value('num');

            if ($num <= 0) {
                return tips('error',-1,'仓票数量不足');
            }
            // 修改支付方式
            $orderInfo -> pay_type = $request -> input('pay_type','wxpay');
            // 实例化支付类
            $app = Factory::payment(config('easywechat.mini_pay'));
            $jssdk = $app->jssdk;
            // 微信统一下单
            $res = $app -> order -> unify([
                'body' => '仓票购买',
                'out_trade_no' => $orderInfo -> unique,
                //'total_fee' => $orderInfo->pay_money * 100,
                'total_fee' => $orderInfo->pay_money * 100,
                'notify_url' => 'https://x1.kagaro.com/api/wx-notify/stock',
                'trade_type' => 'JSAPI',
                'openid' => $userInfo -> wx_openid,
            ]);
            if($res['result_code'] == 'FAIL'){
                throw new \Exception($res['err_code_des']);
            }
            // 获取预支付id后重新签名
            $res = $jssdk->bridgeConfig($res['prepay_id'],false);
            $orderInfo -> save();
            // 提交
            \DB::commit();
            return tips('success',200,'支付成功',$res);
        }catch (\Exception $e){
            \DB::rollBack();
            return tips('error',-1,$e->getMessage());
        }

    }


    /**
     *
     * @author Bryant
     * @date 2020-11-24 15:44
     *
     * 线下支付
     */
    public function unlinePay(Request $request)
    {
        //$pay_imgs =  $request->pay_imgs ?? [];
        $userInfo = $this->userInfo(); // 获取用户信息
        $order_id = $request->order_id;
        if (!$order_id) {
            return tips('erroe',-1,'参数错误');
        }
        $orderInfo = StockOrderService::getOrderInfo($order_id);
        if (!$orderInfo) {
            return tips('error',-1,'订单不存在');
        }
        if ($orderInfo['status'] == 1) {
            return tips('error',-1,'订单已经支付');
        }

        $num = StockModel::where('id',$orderInfo->stock_id)->value('num');

        if ($num <= 0) {
            return tips('error',-1,'仓票数量不足');
        }

        if ($orderInfo->pay_money > $userInfo->compony_money) {
            return tips('error',-1,'您的公司余额不足暂时没法购买');
        }

        // 给用户加上对应的仓票数据
        $data = [
            'uid' => $orderInfo->uid,
            'order_id' => $orderInfo->id,
            'get_money' => $orderInfo->get_money,
            'pay_money' => $orderInfo->pay_money,
            'use_month' => $orderInfo->use_month,
            'use_year' => $orderInfo->use_year,
            'created_at' => date('Y-m-d H:i:s',time())
        ];
        UserStockService::set($data);
        // 减库存
        StockModel::where('id',$orderInfo->stock_id)->decrement('num',1);
        StockModel::where('id',$orderInfo->stock_id)->increment('sales',1);
        $orderInfo -> status = 1;// 改为已支付
        $res = $orderInfo -> save();
        // 扣减公司余额
        if ($res) {
            $userInfo->compony_money -=$orderInfo->pay_money;
            if ($userInfo->compony_id) {
                $ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                UserModel::whereIn('uid',$ids)->update([
                    'compony_money'=> $userInfo->compony_money,
                ]);
            }else {
                //$ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                UserModel::where('uid',$userInfo->uid)->update([
                    'compony_money'=> $userInfo->compony_money,
                ]);
            }

            UserBillService::expend('购买仓票', $userInfo->uid, 'compony_money', 'stock', $orderInfo->pay_money,$orderInfo->id,$userInfo['compony_money'], '您购买仓票扣减余额'.$orderInfo->pay_money, 1, 0,0);
        }
        return tips('success',200,'支付成功');
    }


    /**
     *
     * @author Bryant
     * @date 2021-04-01 17:27
     *
     * 线下支付
     *
     */
    public function offlinePay(Request $request)
    {
        try {
            $stock_id = $request->stock_id;
            $userInfo = $this->userInfo();
            $pay_imgs = $request->pay_imgs ?? [];
            $num = $request->num ?? 1;
            if (!$stock_id) {
                return tips('error',-1,'参数错误');
            }
            $pay_type = $request->pay_type ?? 'balance';
            if (!$pay_type) {
                return tips('error',-1,'请选择支付方式');
            }
            $info = StockService::getStockInfo($stock_id);
            if (!$info) {
                return tips('error',-1,'仓票状态不允许操作');
            }
            if ($info['num'] < $num) return tips('error',-1,'仓票数量不足');
            $stockOrder = new StockOrderModel();
            $stockOrder -> uid = $userInfo ->uid;
            $stockOrder -> stock_id = $stock_id;
            $stockOrder -> get_money = $info['get_money'];
            $stockOrder -> pay_money = $info['pay_money']*$num;
            $stockOrder -> use_month = $info['use_month'];
            $stockOrder -> use_year = $info['use_year'];
            $stockOrder -> status = 0; //初始化
            $stockOrder -> pay_type = $pay_type;
            $stockOrder -> created_at = date('Y-m-d H:i:s',time());
            $stockOrder -> unique = 'ST'.time().mt_rand(00000,99999);
            $stockOrder -> num = $num;
            $stockOrder -> pay_imgs = $pay_imgs;
            $stockOrder -> examine_status = 1;
            $stockOrder -> save();
            $order_id = $stockOrder->id;
            if (!$order_id) {
                throw new \Exception('下单失败');
            }
            return tips('success',200,'下单成功',[
                'order_id' => $order_id,
                'pay_money' =>  $stockOrder -> pay_money,
                'info' => $info,
            ]);
        }catch(\Exception $e){
            return tips('error',-1,$e->getMessage());
        }
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-26 16:24
     *
     * 支付成功
     */
    public function payStockSuccess(Request $request)
    {
        // 开启事务
        \DB::beginTransaction();
        try{
            $app = Factory::payment(config('easywechat.mini_pay'));
            $response = $app -> handlePaidNotify(function($message,$fail){
                // 回调数据
                Log::info('充值订单支付回调数据：' . json_encode($message));
                $payNumber = $message['out_trade_no'];
                // 查找订单是否存在
                $orderObj = RechargeOrderModel::where('pay_number','=',$payNumber)
                    -> first();
                if(!$orderObj){
                    Log::error("找不到支付单号 - {$payNumber} 的充值订单");
                    return $fail('找不到相关订单信息');
                }
                // 验证订单是否已经支付
                if($orderObj -> order_status != 2){
                    Log::info('订单已支付 - ' . $payNumber);
                    return true;
                }
                // 根据获取会员信息
                $memberObj = $this -> getMemberForId($orderObj -> member_id);
                // 修改会员资金信息
                $this -> editMoney($memberObj,$orderObj);
                //增加购买量
                $orderItemObj=CourseOrderItemModel::where('order_id',$orderObj->id)->first();

                $res=CourseModel::where('id',$orderItemObj->course_id)->increment('sale_count');
                // 修改订单状态
                $orderObj -> order_status = 1;
                $orderObj -> pay_time = date('Y-m-d H:i:s');
                $orderObj -> wx_transaction_id = $message['transaction_id'];
                $orderObj -> is_refund = 3;
                $orderObj -> pay_total_fee = $message['total_fee'];
                $orderObj -> save();
                return true;
            });
            \DB::commit();
            return $response;
        }catch (\Exception $e){
            \DB::rollBack();
            Log::error('微信支付回调异常：' . $e -> getMessage());
            $xml = "<xml>
                        <return_code>
                            <![CDATA[FAIL]]>
                        </return_code>
                        <return_msg>
                            <![CDATA[支付异常]]>
                        </return_msg>
                    </xml>";
            return $xml;
        }
    }



}
