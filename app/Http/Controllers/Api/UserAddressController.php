<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Service\UserAddressService;
use Illuminate\Http\Request;
use App\Http\Requests\Api\Address\SaveRequest as AddressRequest;

class UserAddressController extends BaseController
{
    public function __construct()
    {
        $this -> middleware('member');
    }
    /**
     *
     * @param AddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-06 14:35
     *
     * 保存地址
     */
    public function setAddress(AddressRequest $request)
    {
        $userInfo = $this->userInfo();
        $address_id = $request->address_id;
        $data = [
            'uid' => $userInfo -> uid,
            'real_name' => $request->real_name,
            'province' => $request->province,
            'phone' => $request->phone,
            'city' => $request->city,
            'district' => $request->district,
            'detail' => $request->detail,
            'is_default' =>$request->is_default,
            'post_code' => $request->post_code,
            'add_time' => time(),
        ];
        $res = UserAddressService::setAddress($userInfo->uid,$data,$address_id);
        if (!$res) {

            return tips('error',-1,'保存失败');
        }
        return tips('success',200,'保存成功');
    }

    /**
     *
     * @param AddressRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-06 14:35
     *
     * 保存地址
     */
    public function editAddress(AddressRequest $request)
    {
        $userInfo = $this->userInfo();
        $address_id = $request->address_id;
        $data = [
            'uid' => $userInfo -> uid,
            'real_name' => $request->real_name,
            'province' => $request->province,
            'phone' => $request->phone,
            'city' => $request->city,
            'district' => $request->district,
            'detail' => $request->detail,
            'is_default' =>$request->is_default,
            'post_code' => $request->post_code,
            'add_time' => time(),
        ];
        $res = UserAddressService::setAddress($userInfo->uid,$data,$address_id);
        if (!$res) {

            return tips('error',-1,'保存失败');
        }
        return tips('success',200,'保存成功');
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-06 14:36
     * 设置默认值
     */
    public function setDefault(Request $request)
    {
        $userInfo = $this->userInfo();
        $address_id = $request->address_id;
        $result = UserAddressService::setDefault($userInfo->uid,$address_id);
        if (!$result) {
            return tips('error',-1,'设置失败');
        }
        return tips('success',200,'设置成功');

    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-06 14:52
     *
     * 获取地址详情
     */
    public function getDetailt(Request $request)
    {
        $userInfo = $this->userInfo();
        $address_id = $request->address_id;
        $result = UserAddressService::getDetailt($userInfo->uid,$address_id);
        if (!$result) {
            return tips('error',-1,'获取失败');
        }
        return tips('success',200,'获取成功',$result);

    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-12 13:43
     *
     * 删除地址
     */
    public function deleteAddress(Request $request)
    {
        $address_id = $request->address_id;
        $address_id = explode(',',$address_id);
        $userInfo = $this->userInfo();
        if (!$address_id) {
            return tips('error',-1,'请选择需要删除的地址');
        }
        $res = UserAddressService::deleteAddress($userInfo->uid,$address_id);

        if (!$res) {
            return tips('error',-1,'删除失败');
        }
        return tips('success',200,'删除成功');

    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-12 14:46
     *
     * 地址列表
     */
    public function addressList(Request $request)
    {
        $userInfo = $this->userInfo();
        $list = UserAddressService::addressList($userInfo->uid);
        return tips('success',200,'获取成功',$list);
    }
}
