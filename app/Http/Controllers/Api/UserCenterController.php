<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Lib\OpenWechat;
use App\Models\DriverModel;
use App\Models\User;
use App\Models\UserAppoint;
use App\Models\UserBillModel;
use App\Models\UserModel;
use App\Models\UserStock;
use App\Service\GroupDataService;
use App\Service\HttpService;
use App\Service\MasterService;
use App\Service\UserBillService;
use Illuminate\Http\Request;

class UserCenterController extends BaseController
{
    public function __construct()
    {
        $this->middleware('member');

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-02 10:39
     *
     * 获取用户信息
     */
    public function getUserInfo()
    {
        $userInfo = $this->userInfo();

        if (!$userInfo) {

            return tips('error',-1,'用户信息异常');
        }
        return tips('success',200,'获取成功',$userInfo);
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-16 15:41
     *
     *
     * 用户的推广码
     */
    public function getQrcode()
    {
        $userInfo = $this->userInfo();
        $qr_path = "./uploads/";
        if(!file_exists($qr_path.'share/')){
            mkdir($qr_path.'share/', 0700,true);//判断保存目录是否存在，不存在自动生成文件目录
        }
        $filename = 'share/'.time().'.png';
        $file = $qr_path.$filename;
        $open = new OpenWechat(config('app.WX_OPEN_APP_ID'),config('app.WX_OPEN_APP_SECRET'));
        $access = $open->getAccent();
        $access = json_decode($access,true);
        $access_token= $access['access_token'];
        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token='.$access_token;
        $qrcode = json_encode(array(
            'scene'			=> 'uid='.$userInfo->uid,//二维码所带参数
            'width'			=> 300,
            'path'			=> 'packageA/poster/poster',//二维码跳转路径（要已发布小程序）
            'auto_color'	=> true
        ));
        $result = httpCurl($url,'post',$qrcode);//请求微信接口
        // 错误码
        $errcode = json_decode($result,true)['errcode'];
        // 错误信息
        $errmsg = json_decode($result,true)['errmsg'];
        if($errcode) {

            return tips(-1,'error',$errmsg);
        }
        $banner = GroupDataService::getData('routine_spread_banner') ?? [];//TODO 背景图
        $res = file_put_contents($file,$result);//将微信返回的图片数据流写入文件
        //判断是否已存在该二维码图片
        $img =UserModel::where('uid',$userInfo->uid)->value('share_qrcode');

        if($res===false){
            return tips(-1,'error','生成二维码失败');
        }else{
            //返回图片地址链接给前端，入库操作
            $fName = '/uploads/'.$filename;

            if (isset($img) && !empty($img)){
                //返回图片链接信息
                return tips('success',200,['background'=>$banner,'qrcode'=>$img]);
            }else{
                //否则，添加一条新的记录
                $dataArr['share_qrcode'] = $fName ;
                UserModel::where('uid',$userInfo->uid)->update($dataArr);
                return tips('success',200,['background'=>$banner,'qrcode'=>$fName]);
            }
        }
    }


    /**
     *
     * @author Bryant
     * @date 2020-11-16 15:42
     *
     * 根据二维码绑定关系
     */
    public function bindAgent(Request $request)
    {
        $agent_id = $request->agent_id; // 二维码的参数
        $userInfo = $this->userInfo();  // 获取用户信息
        // 判断是否有上级
        $first_id = $userInfo->spread_uid;
        if ($first_id) {
            return tips('error',-1,'您已经是别人的下级请勿重复绑定');
        }
        $res = UserModel::where('uid',$userInfo->uid)->update([
            'spread_uid' => $agent_id,
            'spread_time' => time(),
        ]);

        if (!$res) {

            return tips('error',-1,'绑定失败');
        }
        // 给上级加上推广人数
        UserModel::where('uid',$agent_id)->increment('spread_count',1);
        return tips('success',200,'绑定成功');

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-16 16:27
     *
     *
     * 分销中心
     */
    public function agentCenter(Request $request)
    {
        $userInfo = $this->userInfo();
        $userInfo->uid = 147;
        // 今天开始时间和结束时间
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        // 今日佣金
        $where = [$beginToday,$endToday];
        $countToday = UserBillService::getBill($userInfo->uid,'now_money','brokerage',$where);
        // 总佣金
        $countSum = UserBillService::getBill($userInfo->uid,'now_money','brokerage');
        // 一级下线人数
        $memSum = UserModel::where('spread_uid',$userInfo->uid)->count('uid');
        // 二级下线人数
        $second_id = UserModel::where('spread_uid',$userInfo->uid)->get('uid')->toArray();
        $secondSum = UserModel::whereIn('spread_uid',$second_id)->count('uid');
        // 用户推广列表
        $type = $request->type ?? 2; //
        if ($type == 1) {
            // 一级推广列表
            $userList = UserModel::where('spread_uid',$userInfo->uid)->paginate();
            if ($userList) {
                foreach ($userList->items() as $key=>$value){
                    $sum = UserBillModel::where('send_id',$value->uid)->where('uid',$userInfo->uid)->sum('number');
                    $value->sum = $sum;
                }
            }
        } else {
            //二级推广列表
            $second_id = UserModel::where('spread_uid',$userInfo->uid)->get('uid')->toArray();
            $userList = UserModel::whereIn('spread_uid',$second_id)->paginate();
            if ($userList) {
                foreach ($userList->items() as $key=>$value){
                    $sum = UserBillModel::where('send_id',$value->uid)->where('uid',$userInfo->uid)->sum('number');
                    $value->sum = $sum;
                }
            }
        }
        return tips('success',200,'获取成功',[
            'memSum' => $memSum,
            'secondSum' => $secondSum,
            'countToday' => $countToday,
            'countSum' => $countSum,
            'userList' => $userList,
        ]);

    }


    /**
     *
     * @author Bryant
     * @date 2020-12-04 18:14
     *
     * 用户的钱包
     */
    public function getUserMoney()
    {
        $userInfo = $this->userInfo();

        $stockNum = UserStock::where('uid',$userInfo->uid)->where('status',0)->count('id');

        return tips('success',200,'获取成功',[
            'userInfo' => $userInfo,
            'stockNum' => $stockNum,
        ]);
    }


    /**
     *
     * @author Bryant
     * @date 2020-12-04 18:47
     *
     * 公司余额的记录
     */
    public function getComponyMoney(Request $request)
    {
        $userInfo = $this->userInfo();
        $start_time =$request->start_time;
        $end_time = $request->end_time;
        $list = UserBillService::getBillList($userInfo->uid,$category='compony_money',$start_time,$end_time);
        if ($list) {
            foreach($list->items() as $key=>&$value){

                $value['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
            }
        }
        return tips('success',200,'获取成功',$list);
    }


    /**
     *
     * @author Bryant
     * @date 2020-12-07 10:14
     *
     * 个人余额记录
     */
    public function getNowMoney(Request $request)
    {
        $userInfo = $this->userInfo();
        $start_time = $request->start_time;
        $end_time = $request->end_time;
        $list = UserBillService::getBillList($userInfo->uid,$category='now_money',$start_time,$end_time);
        if ($list) {
            foreach($list->items() as $key=>&$value){

                $value['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
            }
        }
        return tips('success',200,'获取成功',$list);
    }

    /**
     *
     * @author Bryant
     * @date 2020-12-07 11:49
     *
     * 用户预约
     */
    public function userAppointment(Request $request)
    {
        $userInfo = $this->userInfo();
        $data = [
            'uid' => $userInfo->uid,
            'name' => $request->name,
            'phone' => $request->phone,
            'use_space' => $request->use_space,
            'user_area' => $request->user_area,
            'use_time' => $request->use_time ?? 0,
            'demand_type' => $request->demand_type ?? 0,
            'lay_service' => $request->lay_service ?? 0,
            'logistics_service' => $request->logistics_service ?? 0,
            'lay_product' => $request-> lay_product ?? 0,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $res = UserAppoint::insert($data);
        if (!$res) {
            return tips('error',-1,'预约失败');
        }
        return tips('success',200,'预约成功');
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2021-01-09 18:12
     *
     * 公司余额转化为结算余额
     */
    public function changeMoney()
    {
        $userInfo = $this->userInfo();
        $userObj = UserModel::where('uid',$userInfo->uid)->first();
        $change_money = request('change_money');
        if ($change_money > $userObj->compony_money) {
            return tips('error','error','您的公司余额不足');
        }

        // 加上客户的结算余额
        $urls = "https://w2.kagaro.com/xcx/user/save_wx";
        $data = [
            'mobile' => $userObj->phone,
            'amount' => $change_money,
        ];
        $ress = httpCurl($urls,'post',$data);
        $result = json_decode($ress,true);
        if ($result['code'] == 200) {
            $userInfo->compony_money -=$change_money;
            if ($userInfo->compony_id) {
                $ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                UserModel::whereIn('uid',$ids)->update([
                    'compony_money'=> $userInfo->compony_money,
                ]);
            }else {
                //$ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                UserModel::where('uid',$userInfo->uid)->update([
                    'compony_money'=> $userInfo->compony_money,
                ]);
            }
            // 加入用户账单表
            UserBillService::expend('扣减转化余额', $userInfo->uid, 'compony_money', 'change', $change_money,$userInfo->uid,$userInfo->compony_money ?? 0, '您扣减转化余额'.$change_money, 1, 0,0);

            UserBillService::expend('获得转化余额', $userInfo->uid, 'account_money', 'stock', $change_money,$userInfo->uid,$userInfo->account_money ?? 0, '您获得转化余额'.$change_money, 1, 1,0);
            return tips('success',200,'转化成功');
        } else {
            return tips('error',-1,'转化失败');
        }

    }

    /**
     *
     * @author Bryant
     * @date 2021-01-15 14:23
     *
     *
     * 获取公司id
     */
    public function getComponyId(Request $request)
    {
        $userInfo = $this->userInfo();
    }

    /**
     *
     * @author Bryant
     * @date 2021-02-27 17:02
     *
     * 获取司机信息
     */
    public function getDriver(Request $request,DriverModel $driver,UserModel $userModel)
    {
        $userInfo = $this->userInfo();
        $data = [
            'uid' => $userInfo['uid'],
            'name' => $request->name,
            'link_phone' => $request->link_phone,
            'car_number' => $request->car_number,
            'front_image' => $request->front_imgge,
            'back_image' => $request->back_iamge,
            'head_img' => $userInfo['avatar'],
            'front_card' => $request->front_card,
            'back_card' => $request->back_card,
            'card_number' => $request->card_number,
        ];
        $res = $driver->insert($data);

        if ($res) {
            // 将用户的身份变为司机
            $userModel->where('uid',$userInfo->uid)->update(['is_driver'=>1]);
            return tips('success',200,'成功');
        } else {
            return tips('error',-1,'失败');
        }
    }



}
