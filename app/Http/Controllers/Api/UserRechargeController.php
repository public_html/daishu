<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\UserRechargeModel;
use App\Service\UserRechargeService;
use EasyWeChat\Factory;
use Illuminate\Http\Request;

class

UserRechargeController extends BaseController
{

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-27 18:09
     *
     * 充值下单
     */
    public function rechargeOrder(Request $request)
    {
        $userInfo = $this->userInfo();// 获取用户信息
        $recharge_type = $request->recharge_type ?? 'wxpay';
        $money = $request->money;
        if ($money<=0) {
            return tips('error',-1,'请填写正确的金额');
        }
        $pay_img = $request->pay_img;
        $examine_status = $request->examine_status ?? 0; // 如果是 线下提交则为1
        $data = [
            'uid' => $userInfo->uid,
            'order_id' => 'RE'.time().mt_rand(00000,99999),
            'paid' => 0,
            'price' => $money,
            'pay_imgs' => json_encode($pay_img),
            'recharge_type' => $recharge_type,
            'examine_status' => $examine_status,
            'add_time' => time(),
        ];
        $order_id = UserRechargeService::set($data);

        if (!$order_id) {
            return tips('error',-1,'创建失败');
        }

        return tips('success',200,'创建成功',$order_id);
    }


    /**
     *
     * @author Bryant
     * @date 2020-11-28 9:41
     *
     * 微信支付充值订单
     */
    public function wxPay(Request $request): \Illuminate\Http\JsonResponse
    {   try {
            $order_id = $request->order_id;
            $userInfo = $this->userInfo();
            if (!$order_id) {
                throw new \Exception('参数错误');
            }
            // 详情
            $rechargeInfo = UserRechargeService::getInfo($order_id);
            if ($rechargeInfo['paid'] !=0) {
                throw new \Exception('订单已支付');
            }

            // 实例化支付类
            $app = Factory::payment(config('easywechat.mini_pay'));
            $jssdk = $app->jssdk;
            // 微信统一下单
            $res = $app -> order -> unify([
                'body' => '代叔充值',
                'out_trade_no' => $rechargeInfo -> order_id,
                'total_fee' => $rechargeInfo->price * 100,
                'notify_url' => 'https://x1.kagaro.com/api/wx-notify/recharge',
                'trade_type' => 'JSAPI',
                'openid' => $userInfo ->wx_openid,
            ]);
            if($res['result_code'] == 'FAIL'){
                throw new \Exception($res['err_code_des']);
            }
            // 获取预支付id后重新签名
            $res = $jssdk->bridgeConfig($res['prepay_id'],false);
            return tips('success',200,'正在支付',$res);
        }catch(\Exception $e){
            return tips('error',-1,$e->getMessage());
        }
    }

    /**
     *
     * @author Bryant
     * @date 2020-12-10 14:24
     *
     * 充值记录
     */
    public function rechargeList(Request $request)
    {
        $userInfo = $this->userInfo();
        $start_time  = $request->start_time;
        $end_time = $request->end_time;
        $list = UserRechargeService::getList($userInfo->uid,$start_time,$end_time);

        if ($list) {
            foreach($list->items() as &$value) {
                $value['add_time'] = date('Y-m-d H:i:s',$value['add_time']);
            }
        }
        return tips('success',200,'获取成功',$list);
    }


}
