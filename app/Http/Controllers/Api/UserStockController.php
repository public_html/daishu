<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Models\StockOrderModel;
use App\Models\UserModel;
use App\Models\UserStock;
use App\Service\UserBillService;
use App\Service\UserStockService;
use Illuminate\Http\Request;

class UserStockController extends BaseController
{
    //
    public function __construct()
    {
        // 初始化 将过期的仓票改成已过期
        /*$month = date('n');
        $year = date('Y');
        $stock_id = UserStock::where('use_month','<',$month)->where('status',0)->where('use_year','<=',$year)->get('id');
        if ($stock_id) {
            UserStock::whereIn('id',$stock_id)->update([
                'status'=>2,
            ]);
        }*/
        $this -> middleware('member',['except' => ['getStock','getStockInfo']]);
    }


    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author Bryant
     * @date 2020-11-27 15:17
     *
     * 获取用户仓票
     */
    public function getUserStock(Request $request)
    {
        $userInfo = $this->userInfo();
        $type = $request->type ?? 0;
        $where = [];
        // 已用的仓票
        if ($type == 0) {
            $where = [
                'status' => 0,
            ];
            // 过期的
        } else if($type == 1) {
            $where = [
                'status'=> 1,
            ];
        } else if ($type == 2) {
            $where = [
                'status'=> 2,
            ];
        }
        $stockList = UserStockService::getUserStock($where,$userInfo->uid);

        return tips('success',200,'获取成功',$stockList);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-27 15:47
     *
     * 使用仓票
     */
    public function useStock(Request $request)
    {
        $id = $request->id;
        $userInfo = $this->userInfo();
        if (!$id) {
            return tips('error',-1,'参数错误');
        }
        $info = UserStockService::stockInfo($id);
        $is_bill = $request->is_bill ?? 0;
        if ($info->status !=0) {
            return tips('error',-1,'订单不允许操作');
        }
        $month = date('n');
        $year = date('Y');
        // 判断是否是当月使用
        if ($info->use_month > $month && $info->use_year >= $year) {
            return tips('error',-1,'未到时间暂时没法使用仓票');
        }
        $userObj = UserModel::where('uid',$userInfo->uid)->first();
        //$month = date('n');

       /* // 调用三方接口
        $mobile = $userObj->phone;
        $json = array('mobile'=>$mobile);
        $url = 'https://w2.kagaro.com/xcx/user/get_amount';
        $res = httpCurl($url,'post',$json);
        $result = json_decode($res,true);
        if ($result['code'] == 200 && $info->use_month > $month) {
            $account_money = $result['data'];
            if ($account_money < 0) {
                return tips('error',-1,'您的结算余额未结清暂时无法使用仓票');
            }
        } else {
            return tips('error',-1,'暂时无法使用仓票');
        }*/

        // 加上客户的结算余额
        $urls = "https://w2.kagaro.com/xcx/user/save_djq";
        $data = [
            'mobile' => $userObj->phone,
            'amount' => $info['get_money'],
            'code' => $id ?? 0,
        ];
        $ress = httpCurl($urls,'post',$data);
        $results = json_decode($ress,true);
        if ($results['code'] == 200) {
            UserStock::where('id',$id)->where('uid',$userInfo->uid)->update([
                'status' => 1,// 已使用
                'bill_status' => $is_bill, // 申请开具发票
            ]);
            // 加入用户账单表
            UserBillService::expend('获得仓票余额', $userInfo->uid, 'account_money', 'stock', $info['get_money'],$id,$userInfo['account_money'] ?? 0, '您使用仓票获得余额'.$info['get_money'], 1, 1,0);

            return tips('success',200,'使用成功');
        } else {
            return tips('error',-1,'使用失败');
        }

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-27 16:48
     *
     * 获取发票图片
     */
    public function getBillImg(Request $request)
    {
        $id = $request->id;
        $userInfo = $this->userInfo();
        if (!$id) {
            return tips('error',-1,'参数');
        }
        $info = UserStockService::stockInfo($id);

        if ($info['bill_status'] ==0) {
            return tips('error',-1,'您未申请开发票');
        }

        return tips('success',200,'获取成功',$info);
    }

    /**
     *
     * @param Request $request
     * @author Bryant
     * @date 2020-11-27 17:00
     *
     * 申请回收
     */
    public function reviewStock(Request $request)
    {
        $id = $request->id;
        $userInfo = $this->userInfo();
        if (!$id) {
            return tips('error',-1,'参数错误');
        }
        $info = UserStockService::stockInfo($id);
        if ($info->status !=2) {
            return tips('error',-1,'订单不允许操作');
        }
        $res = UserStock::where('id',$id)->where('uid',$userInfo->uid)->update([
            'status' => 3,// 已使用
        ]);

        return tips('success',200,'申请退回中，请等待审核');

    }

    /**
     *
     * @author Bryant
     * @date 2020-12-01 11:59
     *
     * 购买记录
     */
    public function payRecord()
    {
        $userInfo = $this->userInfo();
        $list = StockOrderModel::where('uid',$userInfo->uid)->where('status',1)->paginate();
        return tips('success',200,'获取成功',$list);

    }
}
