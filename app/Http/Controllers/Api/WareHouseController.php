<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Service\WareHouseService;
use Illuminate\Http\Request;

class WareHouseController extends BaseController
{
    public function __construct()
    {
        $this -> middleware('members');
    }

    //
    public function wareList(Request $request,WareHouseService $houseService)
    {
        $list = $houseService->getWare($request);
        if (!$list) {
            return tips('error',-1,'暂无数据');
        }
        return tips('success',200,'成功',$list);
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-28 17:18
     */
    public function getInfo(Request $request,WareHouseService $houseService)
    {
        $userInfo = $this->userInfos();
        $res = $houseService->is_collect($userInfo->uid,$request->id);
        $info = $houseService->getOne($request->id);
        if (!$info) {
            return tips('error',-1,'暂无数据');
        }
        return tips('success',200,'成功',[
            'info' => $info,
            'is_collect' => $res
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-28 18:07
     *
     *
     */
    public function toggleWare(Request $request,WareHouseService $houseService)
    {
        $userInfo = $this->userInfos();
        $uid = $userInfo->uid;
        $ware_id = $request->ware_id;
        if (!$ware_id)  return tips('error',-1,'参数错误');
        $status = $houseService->toggleWare($uid,$ware_id);
        return tips('success','200',$status ? '关注成功':'取消关注成功');

    }

    /**
     *
     * @author Bryant
     * @date 2021-04-30 11:03
     *
     */
    public function searchWare(Request $request,WareHouseService $houseService)
    {
        $name = $request->name;
        $result = $houseService->search($name);
        if (!$result) return tips('error',-1,'暂无数据');
        return tips('success',200,'成功',$result);
    }
}
