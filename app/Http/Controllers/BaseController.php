<?php

namespace App\Http\Controllers;


use App\Models\GoodsCartInfoModel;
use App\Models\GoodsOrderModel;
use App\Models\UserModel;
use App\Service\GoodsAttrService;
use App\Service\GoodsOrderStatusService;
use App\Service\HttpService;
use App\Service\SystemConfigService;
use App\Service\UserBillService;
use App\Service\UserStockService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //返回json
    public function tips($status,$code,$message,$data = [])
    {
        return response()->json([
            'status'  => $status,
            'code'    => $code,
            'msg' => $message,
            'data'    => $data,
        ]);
    }


    /**
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @author Bryant
     * @date 2020-11-26 17:43
     *
     * 用户信息
     */
    protected function userInfo(){

        return auth('member') -> user();
    }

    /**
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     * @author Bryant
     * @date 2020-11-26 17:43
     *
     * 用户信息
     */
    protected function userInfos(){

        return auth('members') -> user();
    }

    public static function paySuccess($orderId, $paytype = 'weixin', $formId = '')
    {

        $order = GoodsOrderModel::where('id', $orderId)->first();
        $resPink = true;
        $res1 = GoodsOrderModel::where('id', $orderId)->update(['paid' => 1, 'pay_type' => $paytype, 'pay_time' => time()]);//订单改为支付
        $cartInfo = GoodsCartInfoModel::where('oid', $order['id'])->get('cart_info', 'unique')->toArray() ?: [];
        foreach ($cartInfo as $k => &$cart) $cart['cart_info'] = json_decode($cart['cart_info'], true);
        $res2 = true;
        foreach ($cartInfo as $k => &$cart) {  //减库存加销量
            $res2 = GoodsAttrService::decProductStock($cart['cart_info']['cart_num'], $cart['cart_info']['productInfo']['id'], isset($cart['cart_info']['productInfo']['attrInfo']) ? $cart['cart_info']['productInfo']['attrInfo']['unique'] : '');
        }
        UserModel::where('uid', $order['uid'])->increment('pay_count', 1);// 用户购买次数加一
        GoodsOrderStatusService::status($order['id'], 'pay_success', '用户付款成功'); // 记录订单状态
        $res = $res1 && $resPink && $res2;
        return false !== $res;
    }

    public static function paySuccesss($orderId, $paytype = 'weixin', $formId = '')
    {
        $order = GoodsOrderModel::where('id', $orderId)->first();
        $resPink = true;
        $res1 = GoodsOrderModel::where('id', $orderId)->update(['paid' => 1, 'pay_type' => $paytype, 'pay_time' => time()]);//订单改为支付
        $cartInfo = GoodsCartInfoModel::where('oid', $order['id'])->get('cart_info', 'unique')->toArray() ?: [];
        foreach ($cartInfo as $k => &$cart) $cart['cart_info'] = json_decode($cart['cart_info'], true);
        $res2 = true;
        foreach ($cartInfo as $k => &$cart) {  //减库存加销量
            $res2 = GoodsAttrService::decProductStock($cart['cart_info']['cart_num'], $cart['cart_info']['productInfo']['id'], isset($cart['cart_info']['productInfo']['attrInfo']) ? $cart['cart_info']['productInfo']['attrInfo']['unique'] : '');
        }
        UserModel::where('uid', $order['uid'])->increment('pay_count', 1);// 用户购买次数加一

        GoodsOrderStatusService::status($order['id'], 'pay_success', '用户付款成功'); // 记录订单状态

        //添加成为推荐人的逻辑的判断
        $user_is_promoter = UserModel::where('uid', $order['uid'])->first();
        if ($user_is_promoter['is_promoter']) {
            // 加入推荐员的奖励
            $agent_id = UserModel::where('uid', $order['uid'])->value('spread_uid');

            if ($agent_id) {
                $getType = SystemConfigService::get('type_reword');// 1比例 2 是直接奖励
                $getType = remove_quote($getType);
                $getType = StrToInt($getType);
                if ($getType == 1) {
                    // 给一级加上对应奖励的钱
                    $rate = SystemConfigService::get('fen_add');
                    (int)$rate = remove_quote($rate);
                    (float)$money = $rate * $order['pay_price'] / 100;// 一级奖励比例

                } else {
                    $money = SystemConfigService::get('fen_add');
                    (float)$money = remove_quote($money);

                }
                UserModel::where('uid', $agent_id)->increment('now_money', $money);
                $userInfo = UserModel::where('uid', $order['uid'])->first();
                $mark = $userInfo['nickname'] . '成功消费' . floatval($order['pay_price']) . '元,奖励推广佣金' . floatval($money);
                UserBillService::expend('获得推广佣金', $agent_id, 'now_money', 'brokerage', $money, $order['id'], $userInfo['now_money'], $mark, 1, 1, $order['uid']);
                $second_agent = UserModel::where('uid', $agent_id)->value('spread_uid'); // 二级奖励
                if ($second_agent) {
                    $getType = SystemConfigService::get('type_reword');// 1比例 2 是直接奖励
                    $getType = remove_quote($getType);
                    $getType = StrToInt($getType);
                    if ($getType == 1) {
                        // 给一级加上对应奖励的钱
                        $rate = SystemConfigService::get('san');
                        (int)$rate = remove_quote($rate);
                        (float)$money = $rate * $order['pay_price'] / 100;// 一级奖励比例
                    } else {
                        $money = SystemConfigService::get('fen_add');
                        (float)$money = remove_quote($money);
                    }
                    UserModel::where('uid', $second_agent)->increment('now_money', $money);
                    $userInfo = UserModel::where('uid', $order['uid'])->first();
                    $mark = $userInfo['nickname'] . '成功消费' . floatval($order['pay_price']) . '元,奖励推广佣金' . floatval($money);
                    UserBillService::expend('获得推广佣金', $second_agent, 'now_money', 'brokerage', $money, $order['id'], $userInfo['now_money'], $mark, 1, 1, $order['uid']);
                }

            }
        }

        $res = $res1 && $resPink && $res2;
        return false !== $res;
    }




}
