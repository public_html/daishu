<?php

namespace App\Http\Controllers;

use App\Models\UserModel;

class DataController extends Controller{

public function text(){
        $list = UserModel::where('compony_money','>',0)->groupBy('compony_id')->get(['phone','compony_money','compony_id'])->toArray();
        foreach ($list as $value) {
            // 加上客户的结算余额
            $urls = "https://w2.kagaro.com/xcx/user/save_wx";
            $data = [
                'mobile' =>$value['phone'],
                'amount' =>$value['compony_money'],
            ];
            $ress = httpCurl($urls,'post',$data);
            $result = json_decode($ress,true);

            if ($result['code'] == 200) {
                if (!$value['compony_id']) {
                    UserModel::where('phone',$value['phone'])->update(['compony_money'=>0]);
                } else {
                    UserModel::where('compony_id',$value['compony_id'])->update(['compony_money'=>0]);
                }
                return true;
            }else {
                return false;
            }
        }
    }

}


