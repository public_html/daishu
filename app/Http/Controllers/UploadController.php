<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadImageRequest;


use Intervention\Image\Facades\Image;

class UploadController extends Controller
{
    /**
     * 上传图片接口
     * @param UploadImageRequest $request
     */
    public function image(UploadImageRequest $request){
        try{
            // 获取图片信息
            $image = $request -> file('image');
            $res = $image -> store(adminSavePath('images'));
           /* $filename=$image->getClientOriginalName();
            $compressImg = Image::make($image)->resize(100,150);//压缩图片
            //$image->move("upload\activityImg",$filename);//移动文件
            $path = public_path("upload\activityImg\compress");
            $result = $compressImg->save($path);*/
            return tips('success',200,[
                'path' => $res,
                'preview_path' => getDomain() . '/upload/' . $res
            ]);

        }catch (\Exception $e){
            return tips('error',-1,$e->getMessage());
        }
    }

    /**
     * 上传图片接口
     * @param UploadImageRequest $request
     */
    public function images(UploadImageRequest $request){
        try{
            // 获取图片信息
            $image = $request -> file('image');
            $res = $image -> store(adminSavePath('images'));
            $filename=$image->getClientOriginalName();
            $compressImg = Image::make($image)->resize(100,150);//压缩图片
            //$image->move("upload\activityImg",$filename);//移动文件
            $path = public_path("upload\activityImg\compress").$filename;
            $result = $compressImg->save($path);
            return tips('success',200,[
                'path' => $res,
                'preview_path' => getDomain() . '/upload/' . $res
            ]);

        }catch (\Exception $e){
            return tips('error',-1,$e->getMessage());
        }
    }
}
