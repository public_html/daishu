<?php

namespace App\Http\Controllers;

use App\Models\GoodsOrderModel;
use App\Models\StockModel;
use App\Models\StockOrderModel;
use App\Models\UserModel;
use App\Models\UserRechargeModel;
use App\Service\UserBillService;
use App\Service\UserStockService;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Log;

class WxNotifyController extends BaseController
{
    /**
     * 充值微信支付回调
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function recharge()
    {
        // 开启事务
        \DB::beginTransaction();
        try {
            $app = Factory::payment(config('easywechat.mini_pay'));
            $response = $app->handlePaidNotify(function ($message, $fail) {
                // 回调数据
                Log::info('充值订单支付回调数据：' . json_encode($message));
                $payNumber = $message['out_trade_no'];

                // 查找订单是否存在
                $orderObj = UserRechargeModel::where('order_id', '=', $payNumber)
                    ->first();
                if (!$orderObj) {
                    Log::error("找不到支付单号 - {$payNumber} 的充值订单");
                    return $fail('找不到相关订单信息');
                }
                // 验证订单是否已经支付
                if ($orderObj->paid != 0) {
                    Log::info('订单已支付 - ' . $payNumber);
                    return true;
                }

                // 充值加入用户公司余额
                //UserModel::where('uid',$orderObj->uid)->increment('compony_money',$orderObj->price);
               /* $userInfo = UserModel::where('uid',$orderObj->uid)->first();
                $userInfo->compony_money +=$orderObj->price;
                if ($userInfo->compony_id) {
                    $ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                    UserModel::whereIn('uid',$ids)->update([
                        'compony_money'=> $userInfo->compony_money,
                    ]);
                }else {
                    //$ids = UserModel::where('compony_id',$userInfo->compony_id)->get('uid');
                    UserModel::where('uid',$userInfo->uid)->update([
                        'compony_money'=> $userInfo->compony_money,
                    ]);
                }*/
                $userObj = UserModel::where('uid',$orderObj->uid)->first();
                // 加上客户的结算余额
                $urls = "https://w2.kagaro.com/xcx/user/save_wx";
                $data = [
                    'mobile' => $userObj->phone,
                    'amount' => $orderObj->price,
                ];
                $ress = httpCurl($urls,'post',$data);
                Log::info('第三方接口数据 - ' . $ress.time());
                $result = json_decode($ress,true);
                if ($result['code'] == 200) {
                    $orderObj->get_status = 1; // 到账
                } else {
                    $orderObj->get_status = 2; // 未到账
                }
                // 加入用户的账单
                UserBillService::expend('获得充值余额', $orderObj->uid, 'compony_money', 'recharge', $orderObj['price'],$orderObj->id,$userObj->compony_money, '您充值获得余额'.$orderObj->price, 1, 1,0);
                // 修改订单状态
                $orderObj->paid = 1;
                $orderObj->pay_time = time();
                $orderObj->wx_transaction_id = $message['transaction_id'];
                $orderObj->save();
                return true;
            });
            \DB::commit();
            return $response;
        } catch (\Exception $e) {
            \DB::rollBack();
            Log::error('微信支付回调异常：' . $e->getMessage());
            $xml = "<xml>
                        <return_code>
                            <![CDATA[FAIL]]>
                        </return_code>
                        <return_msg>
                            <![CDATA[支付异常]]>
                        </return_msg>
                    </xml>";
            return $xml;
        }
    }


    /**
     * 商城订单支付回调
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function goods()
    {
        // 开启事务
        \DB::beginTransaction();
        try {
            $app = Factory::payment(config('easywechat.mini_pay'));
            $response = $app->handlePaidNotify(function ($message, $fail) {
                //file_put_contents('text.txt',json_encode($message));
                // 回调数据
                Log::info('商品订单支付回调数据：' . json_encode($message));
                $payNumber = $message['out_trade_no'];
                // 查找订单是否存在
                $orderObj = GoodsOrderModel::where('unique', '=', $payNumber)
                    ->first();
                if (!$orderObj) {
                    Log::error("找不到支付单号 - {$payNumber} 的商品订单");
                    return $fail('找不到相关订单信息');
                }
                // 验证订单是否已经支付
                if ($orderObj->paid == 1) {
                    Log::info('订单已支付 - ' . $payNumber);
                    return true;
                }
                // 成功后的操作
                $this->paySuccess($orderObj->id,$paytype ='weixin');
                return true;
            });
            \DB::commit();
            return $response;
        } catch (\Exception $e) {
            //file_put_contents('text6.txt',json_encode($e));
            \DB::rollBack();
            Log::error('微信支付回调异常：' . $e->getMessage());
            $xml = "<xml>
                        <return_code>
                            <![CDATA[FAIL]]>
                        </return_code>
                        <return_msg>
                            <![CDATA[支付异常]]>
                        </return_msg>
                    </xml>";
            return $xml;
        }
    }


    /**
     * 仓票订单支付回调
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function stock()
    {
        // 开启事务
        \DB::beginTransaction();
        try {
            $app = Factory::payment(config('easywechat.mini_pay'));
            $response = $app->handlePaidNotify(function ($message, $fail) {
                //file_put_contents('text.txt',json_encode($message));
                // 回调数据
                Log::info('仓票支付回调数据：' . json_encode($message));
                $payNumber = $message['out_trade_no'];
                // 查找订单是否存在
                $orderObj = StockOrderModel::where('unique', '=', $payNumber)
                    ->first();
                if (!$orderObj) {
                    Log::error("找不到支付单号 - {$payNumber} 的仓票订单");
                    return $fail('找不到相关订单信息');
                }
                // 验证订单是否已经支付
                if ($orderObj->order_status == 1) {
                    Log::info('订单已支付 - ' . $payNumber);
                    return true;
                }
                // 给用户加上对应的仓票数据
                $data = [
                    'uid' => $orderObj->uid,
                    'order_id' => $orderObj->id,
                    'get_money' => $orderObj->get_money,
                    'pay_money' => $orderObj->pay_money,
                    'use_month' => $orderObj->use_month,
                    'use_year' => $orderObj->use_year,
                    'created_at' => date('Y-m-d H:i:s',time())
                ];
                UserStockService::set($data);
                // 减库存
                StockModel::where('id',$orderObj->stock_id)->decrement('num',1);
                StockModel::where('id',$orderObj->stock_id)->increment('sales',1);
                $orderObj -> status = 1;// 改为已支付
                $orderObj->save();
                return true;
            });
            \DB::commit();
            return $response;
        } catch (\Exception $e) {
            //file_put_contents('text6.txt',json_encode($e));
            \DB::rollBack();
            Log::error('微信支付回调异常：' . $e->getMessage());
            $xml = "<xml>
                        <return_code>
                            <![CDATA[FAIL]]>
                        </return_code>
                        <return_msg>
                            <![CDATA[支付异常]]>
                        </return_msg>
                    </xml>";
            return $xml;
        }
    }

}
