<?php

namespace App\Http\Requests\Api\Address;

use App\Http\Requests\BaseRequest;

class SaveRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required',
            'real_name' => 'required',
            'province' => 'required',
            'city' => 'required',
            'district' => 'required',
            'detail' => 'required',
        ];
    }


    /**
     * 验证信息
     */
    public function messages()
    {
        return [
            'phone.required' => '收货人手机号码不能为空',
            'real_name.required' => '收货人姓名不能为空',
            'province.required' => '请选择省份',
            'city.required' => '请选择城市',
            'district.required' => '请选择地区',
            'detail.required' => '请填写详细地址',
        ];
    }
}
