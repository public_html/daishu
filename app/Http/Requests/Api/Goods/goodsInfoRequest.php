<?php

namespace App\Http\Requests\Api\Goods;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class goodsInfoRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'goods_id' => 'required',
        ];
    }


    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'goods_id.required' => '商品id不能为空',

        ];
    }
}
