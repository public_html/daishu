<?php

namespace App\Http\Requests\Api\GoodsCart;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart_id' => 'required',
            'goods_number' => 'required|numeric|min:1|max:100'
        ];
    }


    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'cart_id.required' => '请选择需要修改的商品',
            'goods_number.required' => '请输入需要修改后的商品数量',
            'goods_number.numeric' => '输入的商品数量格式不合法',
            'goods_number.min' => '数量不能少于1',
            'goods_number.max' => '数量不能少于100'
        ];
    }
}
