<?php

namespace App\Http\Requests\Api\GoodsCart;

use App\Http\Requests\BaseRequest;

class StoreRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'goods_id' => 'required',
            'goods_number' => 'required|numeric|min:1|max:100'
        ];
    }


    public function messages()
    {
        return [
            'goods_id.required' => '请选择需要添加到购物车的商品',
            'goods_number.required' => '请选择需要添加的数量',
            'goods_number.numeric' => '商品数量格式不正确',
            'goods_number.min' => '最少添加1个',
            'goods_number.max' => '最多只能添加100个'
        ];
    }
}
