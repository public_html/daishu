<?php

namespace App\Http\Requests\Api\Login;

use App\Http\Requests\BaseRequest;

class LoginRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }


    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'username.required' => '请输入用户名',
            'password.required' => '请输入密码'
        ];
    }
}
