<?php

namespace App\Http\Requests\Api\Member2;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'head_img' => 'required',
            'sex' => 'required|in:1,2,3'
        ];
    }


    public function messages()
    {
        return [
            'username.required' => '用户名不能为空',
            'head_img.required' => '头像不能为空',
            'sex.required' => '性别不能为空',
            'sex.in' => '性别不在合法范围'
        ];
    }
}
