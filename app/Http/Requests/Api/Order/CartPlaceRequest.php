<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class CartPlaceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cart_ids' => 'required',
            'address_id' => 'required',
            'pay_type' => 'required|in:1,2,3',
            'xyed_money' => 'required_if:pay_type,3'
        ];
    }


    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'cart_ids.required' => '请选择需要下单的商品',
            'address_id.required' => '请选择下单地址',
            'pay_type.required' => '请选择支付方式',
            'pay_type.in' => '支付方式不合法',
            'xyed_money.required_if' => '请输入使用的额度'
        ];
    }
}
