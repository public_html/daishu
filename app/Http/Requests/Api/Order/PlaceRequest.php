<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class PlaceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'goods_id' => 'required',
            'goods_number' => 'required|numeric|min:1|max:100',
            'address_id' => 'required',
            'pay_type' => 'required|in:1,2,3',
            'xyed_money' => 'required_if:pay_type,3'
        ];
    }


    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'goods_id.required' => '请选择下单商品',
            'goods_number.required' => '请选择下单商品数量',
            'goods_number.numeric' => '商品数量不合法',
            'goods_number.min' => '数量最少为1',
            'goods_number.max' => '数量最大为100',
            'address_id.required' => '请选择下单地址',
            'pay_type.required' => '请选择支付方式',
            'pay_type.in' => '支付方式不合法',
            'xyed_money.required_if' => '请输入你要使用的信用额度'
        ];
    }

}
