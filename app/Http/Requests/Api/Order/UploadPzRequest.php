<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class UploadPzRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required',
            'dkpz_image' => 'required',
            //'zfls_number' => 'required',
            //'zfls_money' => 'required|numeric|min:0.01|max:50000',
            // 'pay_app' => 'required|in:1,2'
        ];
    }


    public function messages()
    {
        return [
            'order_id.required' => '请选择订单',
            'zfls_number.required' => '请输入支付流水号',
            'zfls_money.required' => '请输入流水金额',
            'zfls_money.numeric' => '参数不合法',
            'zfls_money.min' => '最低0.01',
            'zfls_money.max' => '最高5w',
            'pay_app.required' => '请选择支付方式',
            'pay_app.in' => '不合法的支付方式'
        ];
    }
}
