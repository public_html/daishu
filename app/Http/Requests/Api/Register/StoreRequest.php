<?php

namespace App\Http\Requests\Api\Register;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|unique:member,phone',
            'code' => 'required'
        ];
    }


    /**
     * 返回的错误信息
     */
    public function messages()
    {
        return [
            'phone.required' => '手机号码不能为空',
            'phone.unique' => '该手机号码已注册',
            'code.required' => '手机验证码不能为空'
        ];
    }
}
