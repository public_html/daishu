<?php

namespace App\Http\Requests;

class UploadImageRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'image' => 'required|mimes:jpg,png,gif'
            'image' => 'required'
        ];
    }


    public function messages()
    {
        return [
            'image.required' => '请选择需要上传的图片',
            'image.mimes' => '上传图片格式不正确，支持的格式jpg、png、gif'
        ];
    }
}
