<?php
/**
 * Created by PhpStorm.
 * User: chentaohua
 * Date: 2020/3/25
 * Time: 10:17
 */

namespace App\Lib;

class OpenWechat{

    private $appid = '';
    private $appsecret = '';


    public function __construct($appid,$appsecret)
    {
        $this -> appid = $appid;
        $this -> appsecret = $appsecret;
    }


    /**
     * 获取token
     * @param $code
     * @return mixed
     * @throws \Exception
     */
    public function getAccessToken($code){

        $param = [];
        $param[] = 'appid=' .$this->appid;
        $param[] = 'secret=' . $this->appsecret;
        $param[] = 'js_code=' . $code;
        $param[] = 'grant_type=authorization_code';
        $params = implode('&', $param);    //用&符号连起来
        $url = 'https://api.weixin.qq.com/sns/jscode2session' . '?' . $params;    // 拼接的请求地址de";
        $res = httpCurl($url,'get');
        $res = json_decode($res,true);
        if(isset($res['errcode'])){
            throw new \Exception($res['errmsg']);
        }
        return $res;
    }


    /**
     * 获取微信用户信息
     * @param $accessToken
     * @param $openId
     * @return mixed
     * @throws \Exception
     */
    public function getUserInfo($accessToken,$openId){
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$accessToken}&openid={$openId}";
        $res = httpCurl($url,'get');
        $res = json_decode($res,true);
        if(isset($res['errcode'])){
            throw new \Exception($res['errmsg']);
        }
        return $res;
    }

    /**
     * 检验数据的真实性，并且获取解密后的明文.
     * @param $encryptedData string 加密的用户数据
     * @param $iv string 与用户数据一同返回的初始向量
     * @param $data string 解密后的原文
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptData($Key, $encryptedData, $iv, &$data)
    {
        if (strlen($Key) != 24) {
            return -41001;
        }
        $aesKey = base64_decode($Key);


        if (strlen($iv) != 24) {
            return -41002;
        }
        $aesIV = base64_decode($iv);
        $aesCipher = base64_decode($encryptedData);
        $result = openssl_decrypt($aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
        $dataObj = json_decode($result);
        if ($dataObj == NULL) {
            return -41003;
        }
        if ($dataObj->watermark->appid != $this->appid) {
            return -41004;
        }
        $data = $result;
        return 0;
    }

    /**
     *
     * @param HttpService $httpService
     * @return mixed
     * @author Bryant
     * @date 2020-11-16 10:55
     *
     * 获取 account_token
     */
    public function getAccent()
    {
        $appid = $this->appid;//配置appid
        $secret = $this->appsecret;//配置secret
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}";
        $res = httpCurl($url, 'GET');
        return $res;
    }

}