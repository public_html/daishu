<?php

namespace App\Lib\apple;

use App\Lib\apple\JWT;
use Exception;
use Illuminate\Support\Facades\Cache;

/**
 * Decode Sign In with Apple identity token, and produce an ASPayload for
 * utilizing in backend auth flows to verify validity of provided user creds.
 *
 * @package  AppleSignIn\ASDecoder
 * @author   Griffin Ledingham <gcledingham@gmail.com>
 * @license  http://opensource.org/licenses/BSD-3-Clause 3-clause BSD
 * @link     https://github.com/GriffinLedingham/php-apple-signin
 */
class ASDecoder {
    /**
     * Parse a provided Sign In with Apple identity token.
     *
     * @param string $identityToken
     * @return object|null
     * @throws Exception
     */
    public static function getAppleSignInPayload(string $identityToken)
    {
        $identityPayload = self::decodeIdentityToken($identityToken);

        return new ASPayload($identityPayload);
    }

    /**
     * Decode the Apple encoded JWT using Apple's public key for the signing.
     *
     * @param string $identityToken
     * @return object
     */
    public static function decodeIdentityToken(string $identityToken) {
        $payload = JWT::decode($identityToken);
        return $payload;
    }

    /**
     * Fetch Apple's public key from the auth/keys REST API to use to decode
     * the Sign In JWT.
     *
     * @param $kid
     * @return array
     * @throws Exception
     */
    public static function fetchPublicKey($kid) : array {
        if (Cache::has('apple_public_keys')){
            $decodedPublicKeys = Cache::get('apple_public_keys');
        }else{
            $publicKeys = file_get_contents('https://appleid.apple.com/auth/keys');
            $decodedPublicKeys = json_decode($publicKeys, true);
            Cache::put('apple_public_keys', $decodedPublicKeys, 86400);
        }
        if(!isset($decodedPublicKeys['keys']) || count($decodedPublicKeys['keys']) < 1) {
            throw new Exception('Invalid key format.');
        }
        foreach ($decodedPublicKeys['keys'] as $value){
            if ($value['kid'] === $kid){
                $parsedKeyData = $value;
            }
        }
        if(!isset($parsedKeyData)) {
            throw new Exception('kid not found.');
        }
        $parsedPublicKey= JWK::parseKey($parsedKeyData);
        $publicKeyDetails = openssl_pkey_get_details($parsedPublicKey);

        if(!isset($publicKeyDetails['key'])) {
            throw new Exception('Invalid public key details.');
        }

        return [
            'publicKey' => $publicKeyDetails['key'],
            'alg' => $parsedKeyData['alg']
        ];
    }
}

/**
 * A class decorator for the Sign In with Apple payload produced by
 * decoding the signed JWT from a client.
 */
class ASPayload {
    protected $_instance;

    public function __construct($instance) {
        if(is_null($instance)) {
            throw new Exception('ASPayload received null instance.');
        }
        $this->_instance = $instance;
    }

    public function __call($method, $args) {
        return call_user_func_array(array($this->_instance, $method), $args);
    }

    public function __get($key) {
        return $this->_instance->$key;
    }

    public function __set($key, $val) {
        return $this->_instance->$key = $val;
    }

    public function getEmail() {
        return (isset($this->_instance->email)) ? $this->_instance->email : null;
    }

    public function getUser() {
        return (isset($this->_instance->sub)) ? $this->_instance->sub : null;
    }

    public function verifyUser(string $user) {

        return $user === $this->getUser();
    }
}
