<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    // 商品分类表
    protected $table = 'store_category';
}
