<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = 'partner_customer';

    public function progress()
    {
        return $this->hasOne(CustomerProgress::class,'customer_id','id');
    }
}
