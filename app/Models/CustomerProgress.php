<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerProgress extends Model
{
    //
    protected $table = 'customer_progress';
}
