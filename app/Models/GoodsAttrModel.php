<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsAttrModel extends Model
{
    // 商品属性表
    protected $table = 'store_product_attr';

    protected function getAttrValuesAttribute($value)
    {
        return explode(',',$value);
    }
}
