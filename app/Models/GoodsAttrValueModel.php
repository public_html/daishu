<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsAttrValueModel extends Model
{
    // 商品属性表
    protected $table = 'store_product_attr_value';

    public $timestamps = false;
}
