<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCartInfoModel extends Model
{
    //
    protected $table = 'store_order_cart_info';

    protected $fillable = ['oid', 'cart_id','product_id','cart_info','unique'];

    public $timestamps = false;

 /*   public function getCartInfoAttribute($value)
    {
        return json_decode($value,true);
    }*/

}
