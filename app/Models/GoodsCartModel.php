<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCartModel extends Model
{

    // 购物车表
    protected $table = 'store_cart';

    public $timestamps = false;
}
