<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCollectModel extends Model
{

    // 评价表
    protected $table = 'store_product_relation';

    public $timestamps = false;

}
