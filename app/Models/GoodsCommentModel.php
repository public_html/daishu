<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsCommentModel extends Model
{
    //

    protected $table = 'store_product_reply';

    public $timestamps = false;

    protected $primaryKey = "id";

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author Bryant
     * @date 2020-11-17 16:49
     *
     * 获取用户信息
     */
    public function user()
    {
        return $this->belongsTo(UserModel::class,'uid');
    }


    /**
     *
     * @param $value
     * @author Bryant
     * @date 2020-11-17 16:53
     *
     * 图片修改器
     */
    public function setPicsAttribute($value)
    {
        return json_encode($value);
    }

    /**
     *
     * @param $value
     * @return mixed
     * @author Bryant
     * @date 2020-11-17 16:52
     *
     * 图片获取器
     */
    public function getPicsAttribute($value)
    {
        return json_decode($value,true);
    }

    /**
     *
     * @param $value
     * @author Bryant
     * @date 2020-11-23 9:43
     *
     *  时间获取器
     */
    public function getAddTimeAttribute($value)
    {
        return date('Y-m-d',$value);
    }
}
