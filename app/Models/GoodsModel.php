<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsModel extends Model
{
    // 商品表
    protected $table = 'store_product';

    public $timestamps = false;

    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     * @author Bryant
     * @date 2020-11-17 14:42
     *
     * 定义全局的条件
     */
    public function registerGlobalScopes($builder)
    {
        foreach ($this->getGlobalScopes() as $identifier => $scope) {
            $builder->withGlobalScope($identifier, $scope);
        }
        //这里就可以随便添加统一的条件了
        $builder->where('is_show','=',1)->where('is_del','=',0);
        return $builder;
    }

    public function getSliderImageAttribute($value)
    {
        return json_decode($value,true);

    }

}
