<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsOrderModel extends Model
{
    // 订单表
    protected $table = 'store_order';
    public $timestamps = false;

    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder
     * @author Bryant
     * @date 2020-11-17 14:42
     *
     * 定义全局的条件
     */
    public function registerGlobalScopes($builder)
    {
        foreach ($this->getGlobalScopes() as $identifier => $scope) {
            $builder->withGlobalScope($identifier, $scope);
        }
        //这里就可以随便添加统一的条件了
        $builder->where('is_del','=',0);
        return $builder;
    }

    /**
     *
     * @param $value
     * @return mixed
     * @author Bryant
     * @date 2020-11-19 14:56
     *
     * 获取器
     */
    protected function getCartIdAttribute($value)
    {
        return json_decode($value,true);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-13 14:07
     *
     * 关联详情
     */
    public function cartitem()
    {
        return $this -> hasMany(GoodsCartInfoModel::class,'oid');
    }


}
