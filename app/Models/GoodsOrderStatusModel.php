<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsOrderStatusModel extends Model
{
    //
    protected $table = 'store_order_status';
}
