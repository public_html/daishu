<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupDataModel extends Model
{
    //系统数据表
    protected $table = "system_group_data";

    public function getValueAttribute($value)
    {
        return json_decode($value,true);
    }
}
