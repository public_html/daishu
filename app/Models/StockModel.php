<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockModel extends Model
{
    // 仓票表
    protected $table = 'stock';
}
