<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StockOrderModel extends Model
{
    // 仓票订单表
    protected $table = 'stock_order';

    protected $primaryKey = 'id';


}
