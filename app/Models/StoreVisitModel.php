<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreVisitModel extends Model
{
    // 用户浏览记录
    protected $table = 'store_visit';

    public $timestamps = false;
}
