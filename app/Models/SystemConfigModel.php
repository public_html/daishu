<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemConfigModel extends Model
{
    // 配置表
    protected $table = "system_config";
}
