<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemGroupModel extends Model
{
    //系统分组表
    protected $table = 'system_group';
}
