<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddressModel extends Model
{
    // 地址表
    protected $table = 'user_address';

    public $timestamps = false;
}
