<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBillModel extends Model
{
    // 用户账单表
    protected $table = "user_bill";

    public function customer()
    {
        return $this->hasOne(Customer::class,'id','link_id');
    }
}
