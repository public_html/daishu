<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserExtractModel extends Model
{
    // 用户账单表
    protected $table = "user_extract";

}
