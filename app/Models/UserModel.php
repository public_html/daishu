<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserModel extends Authenticatable implements JWTSubject
{
    use Notifiable;


    // 会员表
    protected $table = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname','pwd',
    ];

    protected $primaryKey = "uid";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pwd', 'remember_token',
    ];

    public $timestamps = false;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-13 14:07
     *
     * 关联用户账单
     */
    public function bill()
    {
        return $this -> hasMany(UserBillModel::class,'send_id','uid');

    }

    /**
     *
     * @author Bryant
     * @date 2020-11-23 10:03
     *
     * 时间获取器
     */
    public function getAddTimeAttribute($value)
    {
        return date('Y-m-d',$value);
    }
}