<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeModel extends Model
{
    // 用户
    protected $table = 'user_recharge';

    public $timestamps = false;
}
