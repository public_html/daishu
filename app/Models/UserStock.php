<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStock extends Model
{
    //用户仓票表
    protected $table = 'user_stock';

    public $timestamps = false;
}
