<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WareHouse extends Model
{
    //
    protected $table = 'warehouse';

    public function getSliderImageAttribute($key)
    {
        return json_decode($key);
    }
}
