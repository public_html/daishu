<?php

namespace App\Service;
use App\Models\BannerModel;

class BannerService
{
    protected static $banners;

    function __construct(BannerModel $banner)
    {
        self::$banners = $banner;
    }

    /**
     *
     * @param $where
     * @author Bryant
     * @date 2020-11-02 17:34
     *
     * 根据条件筛选banner
     */
    public function getBanner($where)
    {
        self::$banners->where($where)->get();
    }
}