<?php

namespace App\Service;

use App\Models\Customer;
use App\Models\CustomerProgress;

class CustomerService
{
    /**
     *
     * @param $uid
     * @param $request
     * @return mixed
     * @author Bryant
     * @date 2021-04-23 16:57
     *
     * 录入客户
     */
    public static function create($partner_id,$request)
    {
        $data = [
            'partner_id' => $partner_id,
            'name' => $request->name,
            'mobile' => $request->mobile,
            'created_at' => date('Y-m-d H:i:s',time()),
        ];
        $id =  Customer::insertGetId($data);

        // 插入客户进度表
        $datas = [
            'customer_id' => $id,
            'is_appoint' => 1,
            'is_bargain' => 1,
            'is_get' => 1,
            'created_at' => date('Y-m-d H:i:s',time()),
        ];
        return CustomerProgress::insert($datas);
    }

    /**
     *
     * @param $where
     * @author Bryant
     * @date 2021-04-23 17:32
     *
     * 根据条件查询
     */
    public function getOne($where)
    {
        return Customer::where($where)->first();
    }

    /**
     *
     * @param $customer_id
     * @author Bryant
     * @date 2021-04-25 11:06
     *
     * 查看客户的进度
     */
    public function getProgress($customer_id)
    {
        return CustomerProgress::where('customer_id',$customer_id)->first();
    }

    /**
     *
     * @param $customer_id
     * @author Bryant
     * @date 2021-04-25 16:30
     *
     * 查看客户进度
     */
    public function updateProgress($customer_id,$request)
    {
        $data = [
            'customer_need' => $request->customer_need ?? '',

        ];
        // 预约更新
        $datas = [];
        if ($request->is_appoint == 2) {
            $datas = [
                'is_appoint'=> 2,
                'appoint_time' => date('Y-m-d H:i:s',time()),
            ];
        }
        // 到场更新
        $datass = [];
        if ($request->is_get == 2) {
            $datass = [
                'is_get' => 2,
                'get_time' => date('Y-m-d H:i:s',time()),
            ];
        }
        $data = array_merge($data,$datas,$datass);
        return CustomerProgress::where('customer_id',$customer_id)->update($data);
    }



}
