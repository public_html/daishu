<?php

namespace App\Service;

use App\Models\GoodsAttrModel;

use App\Models\GoodsAttrValueModel;
use App\Models\GoodsModel;

class GoodsAttrService
{

    /**
     *
     * @param $goods_id
     * @author Bryant
     * @date 2020-11-04 15:50
     * 获取商品属性
     */
    public static function getAttr($goods_id)
    {
        $attrDetail =  GoodsAttrModel::where('product_id',$goods_id)->orderBy('attr_values','asc')->get()->toArray()?:[];
        $_values = GoodsAttrValueModel::where('product_id',$goods_id)->get()->toArray()?:[];
        $values = [];
        foreach ($_values as $value){
            $values[$value['suk']] = $value;
        }
        foreach ($attrDetail as $k=>$v){
            $attr = $v['attr_values'];


//            unset($productAttr[$k]['attr_values']);
            foreach ($attr as $kk=>$vv){
                $attrDetail[$k]['attr_value'][$kk]['attr'] =  $vv;
                $attrDetail[$k]['attr_value'][$kk]['check'] =  false;
            }
        }
        return [$attrDetail,$values];
    }


    /**
     *
     * @param $unique
     * @author Bryant
     * @date 2020-11-05 11:24
     *
     * 根据unique 查询商品的attrValue 信息
     */
    public static function getUnique($unique)
    {
        return  GoodsAttrValueModel::where('unique',$unique)->first();
    }

    /**
     *
     * @param $unique
     * @return mixed
     * @author Bryant
     * @date 2020-11-05 16:02
     *
     * 对应属性的库存
     */
    public static function getUniqueStock($unique)
    {
        return  GoodsAttrValueModel::where('unique',$unique)->value('stock');
    }

    /**
     *
     * @param $num
     * @param $productId
     * @param string $unique
     * @return bool
     * @author Bryant
     * @date 2020-11-11 10:43
     *
     * 减库存 加销量
     */
    public static function decProductStock($num,$productId,$unique = '')
    {
        if($unique){
            $res = false !== self::decProductAttrStock($productId,$unique,$num);
            $res = $res && GoodsModel::where('id',$productId)->increment('sales',$num);
        }else{
            $res = false !== GoodsModel::where('id',$productId)->decrement('stock',$num) && GoodsAttrValueModel::where('product_id',$productId)->increment('sales',$num);
        }
        return $res;
    }

    /**
     *
     * @param $productId
     * @param $unique
     * @param $num
     * @return bool
     * @author Bryant
     * @date 2020-11-11 10:47
     *
     * 对应属性减库存加销量
     */
    public static function decProductAttrStock($productId,$unique,$num)
    {
        return false !== GoodsAttrValueModel::where('product_id',$productId)->where('unique',$unique)
                ->decrement('stock',$num) && GoodsAttrValueModel::where('product_id',$productId)->where('unique',$unique)->increment('sales',$num);
    }
}