<?php

namespace App\Service;

use App\Models\GoodsCartInfoModel;

class GoodsCartInfoService
{

    /**
     *
     * @author Bryant
     * @date 2020-11-10 14:19
     *
     * 设置购物车
     */
    public static function setCartInfo($order_id,array $cartInfo)
    {
        $group = [];
        foreach ($cartInfo as $cart){
            $group[] = [
                'oid'=>$order_id,
                'cart_id'=>$cart['id'],
                'product_id'=>$cart['productInfo']['id'],
                'cart_info'=>json_encode($cart),
                'unique'=>md5($cart['id'].''.$order_id)
            ];
        }
        return GoodsCartInfoModel::insert($group);
    }
}