<?php

namespace App\Service;
use app\ebapi\model\store\StoreProduct;
use app\ebapi\model\store\StoreProductAttr;
use App\Models\GoodsCartModel;
use App\Models\GoodsModel;

class GoodsCartService
{
    protected static $goodsCart;

    public function __construct(GoodsCartModel $goodCart)
    {
        self::$goodsCart = $goodCart;
    }


    /**
     *
     * @param $uid
     * @param string $cartIds
     * @param int $status
     * @author Bryant
     * @date 2020-11-05 10:59
     *
     * 获取用户的购物车
     */
    public static function getCartList($uid,$cartIds = '',$status = 0)
    {
        //$productInfoField = 'id,image,slider_image,price,ot_price,vip_price,postage,mer_id,give_integral,cate_id,sales,stock,store_name,store_info,unit_name,is_show,is_del,is_postage,cost';
        $valid = $invalid = [];
        $model = new GoodsCartModel();
        $model = $model->where('uid',$uid)->where('type','product')->where('is_pay',0)->where('is_del',0);
        if(!$status) $model->where('is_new',0);
        if($cartIds) $model->whereIn('id',$cartIds);
        $list = $model->get()->toArray();
        if(!count($list)) return compact('valid','invalid');
        foreach($list as $k=>$cart){
            $product = GoodsModel::where('id',($cart['product_id']))->first();
            $cart['productInfo'] = $product;
            //商品不存在
            if(!$product){
                $model->where('id',$cart['id'])->update(['is_del'=>1]);
                //商品删除或无库存
            }else if(!$product['is_show'] || $product['is_del'] || !$product['stock']){
                $invalid[] = $cart;
                //正常商品
            }else{
                if($cart['product_attr_unique']){
                    $attrInfo = GoodsAttrService::getUnique($cart['product_attr_unique']);
                    //商品没有对应的属性
                    if(!$attrInfo || !$attrInfo['stock'])
                        $invalid[] = $cart;
                    else{
                        $cart['productInfo']['attrInfo'] = $attrInfo;
                        $cart['truePrice'] = (float)UserLevelService::setLevelPrice($attrInfo['price'],$uid,true);
                        $cart['vip_truePrice'] = (float)UserLevelService::setLevelPrice($attrInfo['price'],$uid);
                        $cart['trueStock'] = $attrInfo['stock'];
                        $cart['costPrice'] = $cart['productInfo']['cost'];
                        $cart['productInfo']['image'] = empty($attrInfo['image']) ? $cart['productInfo']['image'] : $attrInfo['image'];
                        $valid[] = $cart;
                    }
                }else{
                    $cart['truePrice'] = (float)UserLevelService::setLevelPrice($cart['productInfo']['price'],$uid,true);
                    $cart['vip_truePrice'] = (float)UserLevelService::setLevelPrice($cart['productInfo']['price'],$uid);
                    $cart['trueStock'] = $cart['productInfo']['stock'];
                    $cart['costPrice'] = $cart['productInfo']['cost'];
                    $valid[] = $cart;
                }
            }
        }

        foreach ($valid as $k=>$cart){
            if($cart['trueStock'] < $cart['cart_num']){
                $cart['cart_num'] = $cart['trueStock'];
                $model->where('id',$cart['id'])->update(['cart_num'=>$cart['cart_num']]);
                $valid[$k] = $cart;
            }
        }

        return compact('valid','invalid');

    }

    /**
     *
     * @param $uid
     * @param $goods_id
     * @param int $cart_num
     * @param string $unique
     * @author Bryant
     * @date 2020-11-05 16:34
     *
     * 加入购物车
     */
    public static function setCart($uid,$goods_id,$cart_num = 1,$unique='',$type='product',$is_new = 0)
    {
        $where = ['type'=>$type,'uid'=>$uid,'product_id'=>$goods_id,'product_attr_unique'=>$unique,'is_new'=>$is_new,'is_pay'=>0,'is_del'=>0];
        $cart = GoodsCartModel::where($where)->first();
        if ($cart) {
            $cart->add_time = time();
            $cart->cart_num = $cart_num;
            $cart->save();
            $res = $cart->id;
        } else {
            $res =  GoodsCartModel::insertGetId(['type'=>$type,'uid'=>$uid,'product_id'=>$goods_id,'product_attr_unique'=>$unique,'is_new'=>$is_new,'is_pay'=>0,'is_del'=>0,'cart_num'=>$cart_num,'add_time'=> time()]);
        }
        return $res;
    }


    /**
     *
     * @param $cart_id
     * @param $cartNUm
     * @param $uid
     * @return mixed
     * @author Bryant
     * @date 2020-11-05 17:40
     *
     * 修改用户的购物车数量
     */
    public static function editUserCart($cart_id,$cartNum,$uid)
    {
        return GoodsCartModel::where('uid',$uid)->where('id',$cart_id)->update(['cart_num'=>$cartNum]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-05 17:44
     *
     * 获取单个购物车信息
     */
    public static function findCart($cart_id)
    {
        return GoodsCartModel::find($cart_id);
    }

    /**
     *
     * @param $cart_id
     * @return mixed
     * @author Bryant
     * @date 2020-11-12 10:18
     *
     */
    public static function deleteCart($cart_id,$uid)
    {
        return GoodsCartModel::where('uid',$uid)->whereIn('id',$cart_id)->delete();
    }


}
