<?php

namespace App\Service;

use App\Models\CategoryModel;
use App\Models\UserModel;

class GoodsCategoryService
{

    /**
     *
     * @author Bryant
     * @date 2020-11-02 15:02
     *
     * 获取所有的一级分类
     */
    public static function getCate($limit = 0)
    {
        if ($limit) {
            return CategoryModel::where('pid',0)->where('is_show',1)->limit($limit)->get();
        } else {
            return CategoryModel::where('pid',0)->where('is_show',1)->get();

        }

    }



}