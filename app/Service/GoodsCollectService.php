<?php

namespace App\Service;

use App\Models\GoodsCollectModel;

class GoodsCollectService
{

    /**
     *
     * @param $uid
     * @param $goods_id
     * @param $relationType
     * @param string $collect
     * @author Bryant
     * @date 2020-11-04 14:29
     * 收藏
     */
    public function collect($uid,$goods_id,$relationType,$collect='product')
    {
        $data= [];
        $data['uid'] = $uid;
        $data['product_id'] = $goods_id;
        $data['type'] = $relationType;
        $data['category'] = $collect;
        $data['add_time'] = time();
        return GoodsCollectModel::insert($data);
    }

    /**
     *
     * @param $uid
     * @param $goods_id
     * @return mixed
     * @author Bryant
     * @date 2020-11-04 14:37
     * 取消收藏
     */
    public function deleteCollect($uid,$goods_id)
    {
        return GoodsCollectModel::where('uid',$uid)->where('product_id',$goods_id)->update(['is_del'=>1]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-04 14:38
     *
     * 获取用户收藏的商品
     */
    public function userCollect($uid)
    {
        return GoodsCollectModel::where('uid',$uid)->paginate();
    }

    /**
     *
     * @param $uid
     * @param $goods_id
     * @return int
     * @author Bryant
     * @date 2020-11-04 15:01
     *
     * 判断是否收藏
     */
    public static function isCollect($uid,$goods_id)
    {
        $res = GoodsCollectModel::where('uid',$uid)->where('product_id',$goods_id)->first();
        if($res){
            return 1;
        }else{
            return 0;
        }
    }



}