<?php

namespace App\Service;

use App\Models\GoodsCommentModel;

class GoodsCommentService
{

    /**
     *
     * @param $goods
     * @author Bryant
     * @date 2020-11-04 10:33
     *
     * 获取商品的评价
     */
    public static function goodsComments($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)
            ->where('is_del',0)
            ->with('user:uid,nickname,avatar')
            ->paginate();
    }

    /**
     *
     * @param $goods_id
     * @author Bryant
     * @date 2020-11-04 16:25
     *
     * 获取所有的评价数量
     */
    public static function getAllCount($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)->count('id');
    }

    /**
     *
     * @param $goods_id
     * @author Bryant
     * @date 2020-11-04 17:06
     *
     * 获取所有评价列表
     *
     */
    public static function getAllReplyList($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id) ->with('user:uid,nickname,avatar')->paginate();
    }

    /**
     *
     * @param $goods_id
     * @author Bryant
     * @date 2020-11-04 16:58
     *
     * 获取商品的好评数
     */
    public static function getGoodCount($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)->where('product_score',5)->count('id');
    }

    /**
     *
     * @return mixed
     * @author Bryant
     * @date 2020-11-04 17:09
     *
     * 获取商品好评列表
     *
     */
    public static function getGoodList($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)->where('product_score',5)->with('user:uid,nickname,avatar')->paginate();
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-04 17:01
     *
     * 有图片的数量
     */
    public static function getImgCount($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)->where('pics','!=','[]')->count();
    }

    /**
     *
     * @param $goods_id
     * @author Bryant
     * @date 2020-11-04 17:11
     *
     * 获取有图片的列表
     */
    public static function getImgList($goods_id)
    {
        return GoodsCommentModel::where('product_id',$goods_id)->where('pics','!=','[]')->with('user:uid,nickname,avatar')->paginate();
    }


    /**
     *
     * @author Bryant
     * @date 2020-11-17 19:25
     *
     * 添加评论
     */
    public static function addComment($data)
    {
        return GoodsCommentModel::insert($data);
    }
}