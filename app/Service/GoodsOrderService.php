<?php

namespace App\Service;

use app\core\model\system\SystemUserLevel;
use app\core\model\user\UserLevel;
use app\ebapi\model\store\StoreOrderStatus;
use app\ebapi\model\store\StorePink;
use App\Models\GoodsCartInfoModel;
use App\Models\GoodsCartModel;
use App\Models\GoodsCommentModel;
use App\Models\GoodsOrderModel;
use App\Models\UserModel;
use Illuminate\Support\Facades\Cache;

class GoodsOrderService
{
    /**
     *
     * @param $uid
     * @param $where
     * @author Bryant
     * @date 2020-11-13 14:04
     *
     * 根据条件查询用户的订单
     */
    public static function getOrderList($uid,$where=[])
    {
        return GoodsOrderModel::where('uid',$uid)
            ->where($where)
            ->with('cartitem')
            ->orderBy('id','desc')
            ->paginate(10);
    }
    /**
     *
     * @param $where
     * @author Bryant
     * @date 2020-11-10 11:15
     *
     * 根据条件获取订单
     */
    public static function getOrder($where)
    {
        return GoodsOrderModel::where($where)->first();
    }


    /**
     *
     * @param $uid
     * @param $cartInfo
     * @param $priceGroup
     * @param array $other
     * @param int $cacheTime
     * @return string
     * @author Bryant
     * @date 2020-11-10 11:14
     *
     * 加入数据
     */
    public static function cacheOrderInfo($uid,$cartInfo,$priceGroup,$other = [],$cacheTime = 60000)
    {
        $key = md5(time());
        Cache::put('user_order_'.$uid.$key,compact('cartInfo','priceGroup','other'),$cacheTime);
        return $key;
    }

    /**
     *
     * @param $data
     * @return mixed
     * @author Bryant
     * @date 2020-11-10 16:39
     *
     * 创建一个订单
     */
    public static function createOrder($data)
    {
        $datas= new GoodsOrderModel();
        $datas -> uid = $data['uid'];
        $datas -> order_id = $data['order_id'];
        $datas -> real_name = $data['real_name'];
        $datas -> user_phone = $data['user_phone'];
        $datas -> user_address = $data['user_address'];
        $datas -> cart_id = $data['cart_id'];
        $datas -> total_num = $data['total_num'];
        $datas -> total_price = $data['total_price'];
        $datas -> total_postage = $data['total_postage'];
        $datas -> coupon_id = $data['coupon_id'];
        $datas -> coupon_price = $data['coupon_price'];
        $datas -> pay_price = $data['pay_price'];
        $datas -> pay_postage = $data['pay_postage'];
        $datas -> deduction_price = $data['deduction_price'];
        $datas -> paid = $data['paid'];
        $datas -> pay_type = $data['pay_type'];
        $datas -> use_integral = $data['use_integral'];
        $datas -> gain_integral = $data['gain_integral'];
        $datas -> mark = $data['mark'];
        $datas -> combination_id = $data['combination_id'];
        $datas -> pink_id = $data['pink_id'];
        $datas -> seckill_id = $data['seckill_id'];
        $datas -> bargain_id = $data['bargain_id'];
        $datas -> cost = $data['cost'];
        $datas -> is_channel = 1;
        $datas -> unique = $data['unique'];
        $datas -> selfmention = $data['selfmention'];
        $datas -> add_time = time();
        $datas ->save();
        return $datas->id;
    }

    /**
     *
     * @return string
     * @author Bryant
     * @date 2020-11-13 15:15
     *
     * 生成唯一订单号
     */
    public static function getNewOrderId()
    {
        //$count = (int) GoodsOrderModel::where('add_time',['>=',strtotime(date("Y-m-d"))],['<',strtotime(date("Y-m-d",strtotime('+1 day')))])->count();
        return 'wx'.date('YmdHis',time()).mt_rand(00000,99999);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-18 16:08
     *
     *
     */
    public static function payAgain($order,$detail = true,$isPic = true)
    {
        if($detail == true && isset($order['id'])){
            $cartInfo = GoodsCartInfoModel::where('oid',$order['id'])->get('cart_info', 'unique')->toArray() ?:[];
            $info=[];
            foreach ($cartInfo as $k=>$cart){
                $cart['cart_info']=json_decode($cart['cart_info'], true);
                $cart['unique']=$k;
                //新增是否评价字段
                $cart['is_reply'] = GoodsCommentModel::where('unique',$k)->count();
                array_push($info,$cart);
                unset($cart);
            }
            $order['cartInfo'] = $info;
        }

        $status = [];
        if(!$order['paid'] && $order['pay_type'] == 'offline' && !$order['status'] >= 2){
            $status['_type'] = 9;
            $status['_title'] = '线下付款';
            $status['_msg'] = '等待商家处理,请耐心等待';
            $status['_class'] = 'nobuy';
        }else if(!$order['paid']){
            $status['_type'] = 0;
            $status['_title'] = '未支付';
            $status['_msg'] = '立即支付订单吧';
            $status['_class'] = 'nobuy';
        }else if($order['refund_status'] == 1){
            $status['_type'] = -1;
            $status['_title'] = '申请退款中';
            $status['_msg'] = '商家审核中,请耐心等待';
            $status['_class'] = 'state-sqtk';
        }else if($order['refund_status'] == 2){
            $status['_type'] = -2;
            $status['_title'] = '已退款';
            $status['_msg'] = '已为您退款,感谢您的支持';
            $status['_class'] = 'state-sqtk';
        }else if(!$order['status']){
            if($order['pink_id']){
                if(StorePink::where('id',$order['pink_id'])->where('status',1)->count()){
                    $status['_type'] = 1;
                    $status['_title'] = '拼团中';
                    $status['_msg'] = '等待其他人参加拼团';
                    $status['_class'] = 'state-nfh';
                }else{
                    $status['_type'] = 1;
                    $status['_title'] = '未发货';
                    $status['_msg'] = '商家未发货,请耐心等待';
                    $status['_class'] = 'state-nfh';
                }
            }else{
                $status['_type'] = 1;
                $status['_title'] = '未发货';
                $status['_msg'] = '商家未发货,请耐心等待';
                $status['_class'] = 'state-nfh';
            }
        }else if($order['status'] == 1){
            $status['_type'] = 2;
            $status['_title'] = '待收货';
            $status['_msg'] = date('m月d日H时i分',StoreOrderStatus::getTime($order['id'],'delivery_goods')).'服务商已发货';
            $status['_class'] = 'state-ysh';
        }else if($order['status'] == 2){
            $status['_type'] = 3;
            $status['_title'] = '待评价';
            $status['_msg'] = '已收货,快去评价一下吧';
            $status['_class'] = 'state-ypj';
        }else if($order['status'] == 3){
            $status['_type'] = 4;
            $status['_title'] = '交易完成';
            $status['_msg'] = '交易完成,感谢您的支持';
            $status['_class'] = 'state-ytk';
        }
        if(isset($order['pay_type']))
            $status['_payType'] = isset(self::$payType[$order['pay_type']]) ? self::$payType[$order['pay_type']] : '其他方式';
        if(isset($order['delivery_type']))
            $status['_deliveryType'] = isset(self::$deliveryType[$order['delivery_type']]) ? self::$deliveryType[$order['delivery_type']] : '其他方式';
        $order['_status'] = $status;
        $order['_pay_time']=isset($order['pay_time']) && $order['pay_time'] != null ? date('Y-m-d H:i:s',$order['pay_time']) : date('Y-m-d H:i:s',$order['add_time']);
        $order['_add_time']=isset($order['add_time']) ? (strstr($order['add_time'],'-')===false ? date('Y-m-d H:i:s',$order['add_time']) : $order['add_time'] ): '';
        $order['status_pic']='';

        if($isPic){
            $order_details_images=GroupDataService::getData('order_details_images') ? : [];
            foreach ($order_details_images as $image){
                if(isset($image['order_status']) && $image['order_status']==$order['_status']['_type']){
                    $order['status_pic']=$image['pic'];
                    break;
                }
            }
        }
        return $order;
    }






}