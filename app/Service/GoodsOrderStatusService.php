<?php

namespace App\Service;


use App\Models\GoodsOrderStatusModel;

class GoodsOrderStatusService
{

    public static function status($oid,$change_type,$change_message,$change_time = null)
    {
        $change_time =  time();
        GoodsOrderStatusModel::insert([
            'oid' => $oid,
            'change_type' => $change_type,
            'change_message' => $change_message,
            'change_time' =>$change_time,
        ]);
    }
}