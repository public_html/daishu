<?php

namespace App\Service;

use App\Models\GoodsModel;


class GoodsService
{


    /**
     *
     * @param $where
     * @author Bryant
     * @date 2020-11-02 15:12
     *
     * 根据条件获取商品
     */
    public function getGoodsList($where,$uid,$order = 'sort desc',$limit = 15)
    {
         $list = GoodsModel::where($where)
            ->orderByRaw($order)
            ->paginate($limit);

         return UserLevelService::setLevelPrice($list->items('data'),$uid);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-02 15:31
     *
     * 获取所有的商品
     */
    public function getAllGoods()
    {
        return  GoodsModel::orderBy('sort','desc')->paginate();
    }

    /**
     *
     * @param $id
     * @author Bryant
     * @date 2020-11-03 11:53
     * 获取商品
     */
    public static function goodsInfo($id)
    {
        return GoodsModel::find($id);
    }

    /**
     *
     * @param $productId
     * @param string $uniqueId
     * @return int
     * @author Bryant
     * @date 2020-11-05 15:42
     * 获取库存
     */
    public static function getProductStock($productId,$uniqueId = '')
    {
       if ($uniqueId) {
           return \App\Service\GoodsAttrService::getUniqueStock($uniqueId) ?? 0;
       } else {
           return GoodsModel::where('id',$productId)->value('stock') ?? 0;
       }
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-09 17:30
     *
     * 获取购物车的价格
     */
    public static function getOrderPriceGroup($cartInfo)
    {
        $storePostage = floatval(SystemConfigService::get('store_postage'))?:0;//邮费基础价
        $storeFreePostage =  floatval(SystemConfigService::get('store_free_postage'))?:0;//满额包邮
        $totalPrice = self::getOrderSumPrice($cartInfo,'truePrice');//获取订单总金额
        $costPrice = self::getOrderSumPrice($cartInfo,'costPrice');//获取订单成本价
        $vipPrice = self::getOrderSumPrice($cartInfo,'vip_truePrice');//获取订单会员优惠金额
        //如果满额包邮等于0
        if(!$storeFreePostage) {
            $storePostage = 0;
        }else{
            foreach ($cartInfo as $cart){
                if(!$cart['productInfo']['is_postage'])//若果产品不包邮
                    $storePostage = bcadd($storePostage,$cart['productInfo']['postage'],2);

            }
            if($storeFreePostage <= $totalPrice) $storePostage = 0;//如果总价大于等于满额包邮 邮费等于0
        }
//        $totalPrice = bcadd($totalPrice,$storePostage,2);
        return compact('storePostage','storeFreePostage','totalPrice','costPrice','vipPrice');
    }





    /**获取某个字段总金额
     * @param $cartInfo
     * @param $key 键名
     * @return int|string
     */
    public static function getOrderSumPrice($cartInfo,$key='truePrice')
    {
        $SumPrice = 0;
        foreach ($cartInfo as $cart){
            $SumPrice = bcadd($SumPrice,bcmul($cart['cart_num'],$cart[$key],2),2);
        }
        return $SumPrice;
    }


}
