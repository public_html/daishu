<?php

namespace App\Service;
use App\Models\GroupDataModel;
use App\Models\SystemGroupModel;
class GroupDataService
{
    public static function getData($config_name,$limit = 0)
    {
        $group_id = SystemGroupModel::where('config_name',$config_name)->value('id');

        $data = GroupDataModel::where('gid',$group_id)->where('status',1)->get();
        return $data;
    }
}