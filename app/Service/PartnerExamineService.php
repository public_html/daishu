<?php

namespace App\Service;

use App\Models\PartnerExamine;

class PartnerExamineService
{
    /**
     *
     * @param $uid
     * @param $request
     * @author Bryant
     * @date 2021-04-23 15:51
     *
     * 申请成为合伙人
     */
    public static function create($uid,$request)
    {
        $data = [
            'uid' => $uid,
            'name' => $request->name,
            'mobile' => $request->mobile,
            'address' => $request->address,
            'type' => $request->type,
            'examine_status' => 1, // 默认为1
            'created_at' => date('Y-m-d H:i:s',time()),
        ];

        return PartnerExamine::insert($data);
    }

    /**
     *
     * @param $uid
     * @param $mobile
     * @author Bryant
     * @date 2021-05-10 17:04
     *
     * 查看是否已经存在记录了
     */
    public function getOne($uid,$mobile)
    {
        return PartnerExamine::where('uid',$uid)->whereIn('examine_status',[1,2])->first();
    }


}
