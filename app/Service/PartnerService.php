<?php

namespace App\Service;


use App\Models\Customer;
use App\Models\Partner;

class PartnerService
{
    public function __construct()
    {
        $this->start_time = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d'), date('Y')));
        $this->end_time = date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d')+1, date('Y'))-1);
    }


    /**
     *
     * @param $partner_id
     * @author Bryant
     * @date 2021-04-25 14:29
     *
     *  获取客户列表
     */
    public function getCustomers($partner_id)
    {
        return Customer::where('partner_id',$partner_id)->paginate();
    }

    /**
     *
     * @param $where
     * @author Bryant
     * @date 2021-04-25 14:38
     * 统计总数
     */
    public function count($partner_id)
    {
        return Customer::where('partner_id',$partner_id)->count();
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-25 14:59
     *
     * 获取 今日的数量
     */
    public function dayCount($partner_id)
    {
        return Customer::where('partner_id',$partner_id)->where('created_at','>=',$this->start_time)->where('created_at','<=',$this->end_time)->count();
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-25 15:26
     *
     * 获取合伙人信息
     *
     */
    public function getPartner($uid)
    {
        return Partner::where('uid',$uid)->first();
    }
}
