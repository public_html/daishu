<?php

namespace App\Service;

use App\Models\StockOrderModel;

class StockOrderService
{

    /**
     *
     * @param $order_id
     * @author Bryant
     * @date 2020-11-24 15:16
     *
     * 获取订单详情
     */
    public static function getOrderInfo($order_id)
    {
        return StockOrderModel::where('id',$order_id)->first();
    }
}