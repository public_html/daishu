<?php

namespace App\Service;

use App\Models\StockModel;

class StockService
{
    /**
     *
     * @author Bryant
     * @date 2020-11-23 17:15
     *
     * 获取符合条件的仓票
     */
    public static function getStock($where)
    {
        if ($where) {

            $list = StockModel::where($where)->where('status',1)->orderBy('use_month','ASC')->paginate();
        } else {
            $list = StockModel::where('status',1)->orderBy('use_month','ASC')->paginate();
        }
        return $list;
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-23 17:19
     *
     * 修改仓票的状态
     */
    public static function updateStock($stock_ids)
    {
        return StockModel::whereIn('id',$stock_ids)->update([
            'status'=>2
        ]);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-23 17:52
     *
     * 获取仓票详情
     */
    public static function getStockInfo($id)
    {
        return StockModel::where('id',$id)->where('status',1)->first();
    }

}