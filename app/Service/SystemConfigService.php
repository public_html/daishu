<?php

namespace App\Service;

use App\Models\SystemConfigModel;

class SystemConfigService
{

    /**
     *
     * @param $key
     * @author Bryant
     * @date 2020-11-11 14:05
     *
     * 获取某个配置的值
     */
    public static function get($key)
    {
        return SystemConfigModel::where('menu_name',$key)->value('value');

    }
}