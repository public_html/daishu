<?php

namespace App\Service;

use App\Models\UserAddressModel;

class UserAddressService
{
    /**
     *
     * @param $uid
     * @return mixed
     * @author Bryant
     * @date 2020-11-12 14:18
     *
     * 获取用户的地址
     */
    public static function addressList($uid)
    {
        return UserAddressModel::where('uid',$uid)->where('is_del',0)->paginate();
    }


    /**
     *
     * @param $uid
     * @param array $data
     * @param int $address_id
     * @author Bryant
     * @date 2020-11-06 13:48
     *
     * 设置 地址
     */
    public static function setAddress($uid,$data = [],$address_id = 0)
    {

        if ($data['is_default'] == 0) {
            // 有更新没有新增
            if ($address_id) {
                $res = UserAddressModel::where('uid',$uid)->where('id',$address_id)->update($data);
            } else {
                $data['uid'] = $uid;
                $list = self::addressList($uid);
                if (!$list) {
                    $data['is_defailt'] = 1;
                }
                $res =UserAddressModel::insert($data);

            }
            // 有默认值
        } else {
            UserAddressModel::where('uid',$uid)->update(['is_default'=>0]);
            if ($address_id) {
                $res = UserAddressModel::where('uid',$uid)->where('id',$address_id)->update($data);
            } else {
                $data['uid'] = $uid;
                $res = UserAddressModel::insert($data);
            }
        }
        return $res;
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-06 13:56
     *
     * 设置默认地址
     */
    public static function setDefault($uid,$address_id  = '')
    {
        UserAddressModel::where('uid',$uid)->update(['is_default'=>0]);

        $res = UserAddressModel::where('uid',$uid)->where('id',$address_id)->update(['is_default'=>1]);
        return $res;
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-06 14:21
     *
     * 获取一个地址
     */
    public static function getDetailt($uid,$address_id = '')
    {

        return UserAddressModel::where('uid',$uid)->where('id',$address_id)->first();
    }

    /**
     *
     * @param $uid
     * @param $address_ids
     * @author Bryant
     * @date 2020-11-12 13:53
     *
     * 删除地址
     */
    public static function deleteAddress($uid,$address_ids)
    {
        return UserAddressModel::where('uid',$uid)->whereIn('id',$address_ids)->delete();
    }

}