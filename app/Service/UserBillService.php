<?php

namespace App\Service;

use App\Models\UserBillModel;

class UserBillService
{
    /**
     *
     * @param $title
     * @param $uid
     * @param $category
     * @param $type
     * @param $number
     * @param int $link_id
     * @param int $balance
     * @param string $mark
     * @param int $status
     * @param int $pm
     * @return mixed
     * @author Bryant
     * @date 2020-11-11 11:42
     *
     * 添加账单
     */
    public static function expend($title,$uid,$category,$type,$number,$link_id = 0,$balance = 0,$mark = '',$status = 1,$pm = 0,$send_id=0)
    {
        $data = [
            'title' =>$title,
            'uid' => $uid,
            'category' => $category,
            'type' =>$type,
            'link_id' => $link_id,
            'number' => $number,
            'balance' => $balance,
            'mark' => $mark,
            'status' => $status,
            'pm' => $pm,
            'add_time' => time(),
            'send_id' => $send_id

        ];
        return UserBillModel::insert($data);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-16 16:43
     *
     * 获取用户推广佣金
     */
    public static function getBill($uid,$category = 'now_money',$type='brokerage',$where = [])
    {
        if ($where) {
            return $count = UserBillModel::where('uid',$uid)
                ->whereBetween('add_time',$where)
                ->where('type',$type)
                ->where('category',$category)
                ->sum('number');
        }else {
            return $count = UserBillModel::where('uid',$uid)
                ->where('type',$type)
                ->where('category',$category)
                ->sum('number');
        }


    }

    /**
     *
     * @param $uid
     * @param $category
     * @param string $type
     * @author Bryant
     * @date 2020-12-07 10:07
     *
     * 获取用户的账单列表
     */
    public static function getBillList($uid,$category,$start_time,$end_time)
    {
        return $list = UserBillModel::where('uid',$uid)
            ->where('category',$category)
            ->whereBetween('add_time',[$start_time,$end_time])
            ->paginate();
    }


    /**
     *
     * @param $uid
     * @param $category
     * @author Bryant
     * @date 2021-05-10 14:24
     *
     * 获取合伙人的收益列表
     */
    public static function getIncome($uid,$category = 'income_money')
    {
        return UserBillModel::with('customer')
            ->where('uid',$uid)
            ->where('category',$category)
            ->paginate();
    }

}
