<?php

namespace App\Service;

use App\Models\UserBillModel;
use App\Models\UserExtractModel;

class UserExtractService
{
    /**
     *
     * @param $uid
     * @author Bryant
     * @date 2021-05-15 15:22
     *
     * 获取提现记录
     */
    public function getExtract($uid)
    {
        UserExtractModel::where('uid',$uid)->where('status',1)->paginate();
    }

    /**
     *
     * @author Bryant
     * @date 2021-05-15 15:30
     *
     *  添加提现记录
     */
    public function create($uid,$request)
    {
        $data = [
            'uid' => $uid,
            'real_name' => $request->real_name,
            'extract_type' => $request->extract_type ?? 'weixin',
            'extract_price' => $request->extract_price,
            'add_time' => time(),
            'status' => 0,
        ];

        return UserExtractModel::insert($data);
    }

}
