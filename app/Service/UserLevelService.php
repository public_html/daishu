<?php

namespace App\Service;

use App\Models\SystemLevelModel;
use App\Models\UserLevelModel;

class UserLevelService
{


    /**
     *
     * @param $uid
     * @param int $grade
     * @return bool
     * @author Bryant
     * @date 2020-11-09 18:07
     *
     */
    public static function getUserLevel($uid,$grade=0)
    {
        $model = new UserLevelModel();
        if ($grade) $model = $model->where('grade', '<', $grade);
        $level = $model->where('uid', $uid)->orderByRaw('grade desc')->first();
        if (!$level) return false;
        if ($level->is_forever) return $level->level_id;
        //会员已经过期
        if (time() < $level->valid_time){
            if($level->status==1){
                $level->status=0;
                $level->save();
            }
            return self::getUserLevel($uid, $level->grade);
        }else
            //会员没有过期
            return $level->level_id;
    }


    /**
     *
     * @param $list
     * @param $uid
     * @param bool $isSingle
     * @return array|int|string
     * @author Bryant
     * @date 2020-11-09 17:43
     *
     *  设置会员的价格
     */
    public static function setLevelPrice($list,$uid,$isSingle=false)
    {
        if(is_object($list)) $list=count($list) ? $list->toArray() : [];
        //dd($list);
        $levelId=UserLevelService::getUserLevel($uid);
        if($levelId){
            $discount=UserLevelService::getUserLevelInfo($levelId,'discount');
            $discount=bcsub(1,bcdiv($discount,100,2),2);
        }else{
            $discount=100;
            $discount=bcsub(1,bcdiv($discount,100,2),2);
        }
        //如果不是数组直接执行减去会员优惠金额
        if(!is_array($list))
            //不是会员原价返回
            if($levelId)
                //如果$isSingle==true 返回优惠后的总金额，否则返回优惠的金额
                return $isSingle ? bcsub($list,bcmul($discount,$list,2),2) : bcmul($discount,$list,2);
            else
                return $isSingle ? $list : 0;
        //当$list为数组时$isSingle==true为一维数组 ，否则为二维
        if($isSingle)
            $list['vip_price']=isset($list['price']) ? bcsub($list['price'],bcmul($discount,$list['price'],2),2) : 0;
        else
            foreach ($list as &$item){

                $item['vip_price']=isset($item['price']) ? bcsub($item['price'],bcmul($discount,$item['price'],2),2) : 0;
            }
        return $list;
    }

    /**
     *
     * @param $levelId
     * @param string $keyName
     * @author Bryant
     * @date 2020-11-09 18:22
     *
     * 获取用户折扣
     */
    public static function getUserLevelInfo($levelId,$keyName = '')
    {
        $model = new SystemLevelModel();
        $info = $model->where('id',$levelId)->first();
        if($keyName) if(isset($info[$keyName])) return $info[$keyName]; else return '';

        return $info;
    }


    /**
     *
     * @param int $id
     * @return mixed
     * @author Bryant
     * @date 2020-11-09 19:11
     *
     * 用户折扣
     */
    public static function getLevelDiscount($id=0)
    {
        $model= new SystemLevelModel();
        if($id) $model=$model->where('id',$id);
        else $model=$model->orderBy('grade asc');
        return $model->value('discount');
    }






}