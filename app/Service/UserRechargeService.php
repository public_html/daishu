<?php

namespace App\Service;

use App\Models\UserRechargeModel;

class UserRechargeService
{
    /**
     *
     * @author Bryant
     * @date 2020-11-27 17:59
     *
     * 创建订单
     */
    public static function set($data)
    {
        return UserRechargeModel::insertGetId($data);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-28 10:01
     *
     * 获取详情
     */
    public static function getInfo($id)
    {
        return UserRechargeModel::where('id',$id)->first();
    }


    /**
     *
     * @param $uid
     * @param $start_time
     * @param $end_time
     * @author Bryant
     * @date 2020-12-10 13:57
     *
     * 充值记录
     */
    public static function getList($uid,$start_time,$end_time)
    {
        return UserRechargeModel::where('uid',$uid)
            ->whereBetween('add_time',[$start_time,$end_time])
            ->where('paid',1)
            ->paginate();
    }
}