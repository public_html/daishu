<?php

namespace App\Service;

use App\Models\UserStock;

class UserStockService
{
    /**
     *
     * @author Bryant
     * @date 2020-11-26 17:26
     *
     * 添加数据
     */
    public static function set($data)
    {
        return UserStock::insert($data);
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-26 17:28
     *
     * 根据条件筛选用户的仓票
     */
    public static function getUserStock($where,$uid)
    {
        return UserStock::where($where)->where('uid',$uid)->paginate();
    }

    /**
     *
     * @author Bryant
     * @date 2020-11-26 17:32
     *
     * 使用仓票
     */
    public static function useStock($data,$uid,$id)
    {
        return UserStock::where('id',$id)->where('uid',$uid)->update($data);
    }


    /**
     *
     * @author Bryant
     * @date 2020-11-27 15:56
     *
     *
     * 获取仓票详情
     */
    public static function stockInfo($id)
    {
        return UserStock::where('id',$id)->first();
    }
}