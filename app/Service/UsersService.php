<?php
namespace App\Service;

use App\Models\UserModel;

class UsersService
{
    protected static $users;

    public function __construct(UserModel $user)
    {
         self::$users= $user;
    }

    /**
     *
     * @param $data
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 13:37
     * 用户添加
     */
    public function store($data)
    {
        return self::$users->insertGetId($data);
    }

    /**
     *
     * @param $id
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 13:37
     *
     * 根据id 获取用户
     */
    public function get($id)
    {
        return self::$users->find($id);
    }

    /**
     *
     * @param $data
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 13:38
     *
     * 用户信息的修改
     */
    public function update($data)
    {
        $user = self::$users->find($data['id']);
        unset($data['id']);
        foreach ($data as $key=>$val){
            $user->$key = $val;
        }
        return $user->save();
    }

    /**
     *
     * @param $id
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 11:59
     *
     * 删除用户
     */
    public function delete($id)
    {
        return self::$users->where('id',$id)->delete();
    }


    /**
     *
     * @param array $where
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 11:59
     *
     * 根据条件查询用户
     */
    public function getUsers($where = [])
    {
        return self::$users->where($where)->first();
    }

    /**
     *
     * @param $fields
     * @param array $where
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 13:58
     *
     * 获取所有的用户
     */
    public function getUserList($fields,$where = [])
    {
        return self::$users->select($fields)->where($where)->paginate();
    }

    /**
     *
     * @param $id
     * @param $userStatus
     * @return mixed
     * @author Bryant
     * @date 2020-11-02 13:41
     *
     * 用户禁用
     */
    public function pullBlack($id,$userStatus)
    {
        $user = self::$users->find($id);
        $user->status = $userStatus;
        return $user->save();
    }

    /**
     * 获取所有用户信息
     * @return Roles[]|\Illuminate\Database\Eloquent\Collection
     * @author:
     * @date: 2019/5/24 16:44
     */
    public function allUsers()
    {
        return self::$users->paginate();
    }

    /**
     *
     * @param $uid
     * @author Bryant
     * @date 2020-11-09 13:38
     *
     * 获取用户的等级
     */
    public function getUserLevel($uid)
    {
        return self::$users->where('id',$uid)->value('level');
    }

}
