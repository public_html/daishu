<?php

namespace App\Service;
use App\Models\WareCollect;
use App\Models\WareHouse;
use Illuminate\Database\Eloquent\Model;

class WareHouseService
{

    function __construct(WareHouse $ware)
    {
        $this->ware = $ware;
    }

    /**
     *
     * @param $type
     * @param $request
     * @author Bryant
     * @date 2021-04-28 14:30
     *
     * 根据条件 查找仓库
     */
    public function getWare($request)
    {
        $where = [];
        $whereRaw = '';
        // 区域筛选
        $distance = $request->distance;
        $field = "*.";
        // 考虑用when
        if ($distance) {
            $lat = $request->lat;
            $lng = $request->lng;
            $whereRaw = "round((
                            6371392.89 * acos (
                            cos ( radians({$lat}) )
                             * cos( radians(lat) )
                             * cos( radians(lng) - radians({$lng}) )
                             + sin ( radians({$lat}) )
                             * sin( radians( lat ) )
                             )), 0) <= {$distance}";

            $field = "*,round((
                            6371392.89 * acos (
                            cos ( radians({$lat}) )
                             * cos( radians(lat))
                             * cos( radians(lng) - radians({$lng}) )
                             + sin ( radians({$lat}))
                             * sin( radians(lat))
                             )), 0) AS distance";
        }

        if ($request->district) {
            $where = [
                ['district','=',$request->district]
            ];
        }
        if ($request->districts){
            $where = [
                ['district','=',$request->districts]
            ];
        }
        // 类型筛选
        if ($request->type) {
            $where  = [
                ['type','=',1]
            ];
        }
        // 价格筛选
        $price = $request->price;
        if ($price){
            $priceBetween = explode('-',$price);
            if(count($priceBetween) == 2){
                $where[] = ['pre_day','>=',$priceBetween[0]];
                $where[] = ['pre_day','<=',$priceBetween[1]];
            }
        }
        // 面积筛选
        $area = $request->area;
        if ($area){
            $areaBetween = explode('-',$area);
            if(count($areaBetween) == 2){
                $where[] = ['area','>=',$areaBetween[0]];
                $where[] = ['area','<=',$areaBetween[1]];
            }
        }
        return  $this->ware::where($where)
            ->when($whereRaw,function($query,$whereRaw){
                return $query->whereRaw($whereRaw);
            })->paginate();
    }

    /**
     *
     * @param $id
     * @author Bryant
     * @date 2021-04-28 14:35
     *
     * 获取一条数据
     */
    public function getOne($id)
    {
        return $this->ware->find($id);
    }

    /**
     *
     * @param $name
     * @author Bryant
     * @date 2021-04-28 16:27
     *
     * 搜索
     */
    public function search($name)
    {
        return $this->ware->where('title','like',"%$name%")->paginate();
    }

    public function toggleWare($uid,$ware_id)
    {
        $collectObj = WareCollect::where(['uid'=> $uid,'warehouse_id'=>$ware_id])->first();
        // 有数据就是删除
        if ($collectObj) {
            $collectObj->delete();
            $status = false;
        } else {
            $teacherCollectModel = new WareCollect();
            $teacherCollectModel->uid = $uid;
            $teacherCollectModel->warehouse_id = $ware_id;
            $teacherCollectModel->status = 1;
            $teacherCollectModel->save();
            $status = true;
        }
        return $status;
    }

    /**
     *
     * @author Bryant
     * @date 2021-04-30 10:02
     *
     * 是否收藏
     */
    public function is_collect($uid,$id)
    {
        $res =  WareCollect::where('uid',$uid)->where('id',$id)->first();

        if ($res) {
            return 2;
        } else {
            return 1;
        }
    }
}
