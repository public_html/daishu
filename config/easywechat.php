<?php
/**
 * Created by PhpStorm.
 * User: chentaohua
 * Date: 2020/3/16
 * Time: 11:15
 */

return [

    'mini' => [
        'app_id' => config('app.WX_APPID_MINI'),
        'secret' => config('app.WX_APPSECRET_MINI'),

        // 下面为可选项
        // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
        'response_type' => 'array',

        'log' => [
            'level' => 'debug',
            'file' => storage_path('easywechat/' . date('Ymd')) . '/wechat.log',
        ],
    ],
    'mini_pay' => [
        // 必要配置
        'app_id'             => config('app.WX_OPEN_APP_ID'),
        'mch_id'             => config('app.WX_MCH_ID'),
        'key'                => 'nxykqjnykjy1602089952xgsnxykqjny',   // API 密钥

        // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
        'cert_path'          => 'wxcert/apiclient_cert.pem', // XXX: 绝对路径！！！！
        'key_path'           => 'wxcert/apiclient_key.pem',      // XXX: 绝对路径！！！！

        'notify_url'         => '',     // 你也可以在下单时单独设置来想覆盖它
    ]
];