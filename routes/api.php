<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// 上传图片接口
Route::any('/v1/upload-image','UploadController@image');
// Api
Route::group(['namespace'=>'Api','prefix'=>'v1'],function(){

    // 用户登录
    Route::post('login','LoginController@Login');
    Route::post('logins','LoginController@Logins');
    // 用户信息
    Route::post('user','LoginController@getUserInfo');
    //微信登录
    Route::post('wx_login','LoginController@wxLogin');
    Route::any('wx_logins','LoginController@wxLogins');
    // 联系我们
    Route::post('link_us','Index@linkUs');
    // 获取经纬度
    Route::post('get_lat','Index@getLat');
    // 测试
    Route::post('get_text','Index@getUrl');
    // 图片
    Route::post('get_image','Index@getImage');
    // 获取仓库经纬度
    Route::post('location','Index@getLocation');
    /******************************************************* 商城相关**********************************/
    Route::post('index','Index@Index');
    Route::post('indexs','Index@Indexs');
    Route::post('get_area','Index@getArea');
    Route::post('textss','Index@getUrl');
    Route::post('shop_index','ShopController@getIndex');// 商城首页
    Route::post('shop_list','GoodsController@goodsList'); // 按条件查询商品
    Route::post('search_goods','GoodsController@searchGoods'); // 搜索商品
    Route::post('goods_info','GoodsController@goodsInfo'); // 商品详情
    Route::post('all_cate','ShopController@getAllCate'); // 所有的分类
    Route::post('goods_comment','GoodsController@goodsComments'); // 商品的评论
    Route::post('all_comment','GoodsController@getAllComments'); // 查看所有的评论
    Route::post('add_comment','GoodsController@addComment'); // 添加购物车
    Route::post('add_collect','GoodsController@goodsCollect'); // 添加收藏
    Route::post('delete_collect','GoodsController@deleteConllect'); // 删除收藏
    /******************************************************* 购物车 ***********************************/
    Route::post('add_cart','GoodsCartController@setCart'); // 加入购物车
    Route::post('user_cart','GoodsCartController@getUserCart'); // 获取用户的购物车
    Route::post('edit_cart','GoodsCartController@editUserCart'); //修改购物车的数量
    Route::post('delete_cart','GoodsCartController@delectCart'); // 删除购物车的商品
    /******************************************************* 订单 *************************************/
    Route::post('now_buy','GoodsCartController@nowBuy'); // 立即购买
    Route::post('confirm_order','GoodsOrderController@orderConfirm'); // 订单页面
    Route::post('create_order','GoodsOrderController@createOrder'); // 创建订单
    Route::post('order_list','GoodsOrderController@getOrderList'); //订单列表
    Route::post('pay_again','GoodsOrderController@payAgain'); // 再次购买
    Route::post('pay_order','GoodsOrderController@payOrder'); //订单支付
    Route::post('que_order','GoodsOrderController@queOrder'); //确认收货
    Route::post('delete_order','GoodsOrderController@deleteOrder'); // 删除订单
    /******************************************************* 地址 *************************************/
    Route::post('set_address','UserAddressController@setAddress'); // 新增和编辑地址
    Route::post('edit_address','UserAddressController@editAddress'); // 新增和编辑地址
    Route::post('set_default','UserAddressController@setDefault'); // 设置默认值
    Route::post('delete_address','UserAddressController@deleteAddress'); // 删除地址
    Route::post('list_address','UserAddressController@addressList'); // 用户的地址管理
    Route::post('detail_address','UserAddressController@getDetailt'); // 地址详情
    /****************************************************** 个人中心 **********************************/
    Route::post('user_info','UserCenterController@getUserInfo'); // 用户信息
    Route::post('user_qrcode','UserCenterController@getQrcode'); // 获取用户推广码
    Route::post('bind_agent','UserCenterController@bindAgent'); // 绑定关系
    Route::post('agent_center','UserCenterController@agentCenter'); // 分销中心
    Route::post('money_bag','UserCenterController@getUserMoney'); // 钱包页面
    Route::post('compony_money','UserCenterController@getComponyMoney'); // 公司余额列表
    Route::post('user_money','UserCenterController@getNowMoney'); // 个人余额列表
    Route::post('user_appiont','UserCenterController@userAppointment'); // 用户预约
    Route::post('user_change','UserCenterController@changeMoney'); // 转化余额
    Route::post('make_driver','UserCenterController@getDriver'); // 成为司机
    /****************************************************** 仓票 *************************************/
    Route::post('get_stock','StockController@getStock'); // 获取仓票
    Route::post('detail_stock','StockController@getStockInfo'); // 获取详情
    Route::post('stock_order','StockController@orderStock'); //仓票下单
    Route::post('offline_stock','StockController@unlinePay'); // 仓票线下支付
    Route::post('offline_stocks','StockController@offlinePay'); // 仓票再次支付
    Route::post('wxpay_stock','StockController@payStock'); // 仓票微信支付
    Route::post('stock_record','UserStockController@payRecord'); // 用户购买记录
    Route::post('user_stock','UserStockController@getUserStock'); //我的仓票
    Route::post('use_stock','UserStockController@useStock'); // 使用仓票
    Route::post('bill_img','UserStockController@getBillImg'); //查看发票
    Route::post('review_stock','UserStockController@reviewStock'); // 仓票回收
    /*********************************************************** 充值 ********************************/
    Route::post('user_recharge','UserRechargeController@rechargeOrder'); // 用户充值
    Route::post('wxpay_recharge','UserRechargeController@wxPay'); // 微信支付
    Route::post('recharge_list','UserRechargeController@rechargeList'); // 充值记录
    /*********************************************************** 合伙人 ****************************/
    Route::post('set_partner','PartnerExamineController@setPartner'); // 申请成为合伙人
    Route::post('set_customer','CustomerController@setCustomer'); // 录入客户资料
    Route::post('customer_progress','CustomerController@customerProgree'); // 查看客户进度
    Route::post('customer_list','CustomerController@customerList'); // 客户列表
    Route::post('update_progress','CustomerController@updateProgree'); // 更新客户进度
    /************************************************************* 仓库******************************/
    Route::post('ware_list','WareHouseController@wareList'); // 列表
    Route::post('ware_info','WareHouseController@getInfo'); // 详情
    Route::post('ware_collect','WareHouseController@toggleWare'); // 收藏
    Route::post('ware_search','WareHouseController@searchWare'); // 搜索

    Route::post('user_pack','PartnerPackController@getIncome');
    Route::post('user_extract','PartnerPackController@getExtract');
    Route::post('set_extract','PartnerPackController@createExtract');

});

// 微信支付回调 - 充值
Route::any('wx-notify/recharge','WxNotifyController@recharge');

// 微信支付回调 - 商城订单
Route::any('wx-notify/goods','WxNotifyController@goods');
// 微信支付回调 - 仓票订单
Route::any('wx-notify/stock','WxNotifyController@stock');
// 跑数据
Route::any('data_get','DataController@text');
